﻿STATE_AVHAVUBHIYA = {
    id = 400
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "xB1AB87" "xC48000" "xCB7C7C" "xCF9191" "xD47638" "xDC1616" }
    traits = {}
    city = "xcb7c7c"
    port = "xdc1616"
    farm = "xd47638"
    mine = "xb1ab87"
    wood = "xcf9191"
    arable_land = 223
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 8
        bg_fishing = 15
    }
    naval_exit_id = 3300
}

STATE_IYARHASHAR = {
    id = 401
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x738FB0" "x92CF98" "xCFB674" "xDB9693" "xDDF4D4" }
    traits = { state_trait_harimari_jungles }
    city = "xdb9693"
    port = "xcfb674"
    farm = "x738fb0"
    wood = "xddf4d4"
    arable_land = 122
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tea_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 16
        bg_fishing = 9
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 1
    }
    naval_exit_id = 3300
}

STATE_SOUTH_GHANKEDHEN = {
    id = 402
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x3681CD" "x55AEA9" "x826F33" "x916B9A" "xA172C0" "xC2D2C7" "xCD36A8" }
    traits = { state_trait_dhenbasana_river state_trait_ghankedhen_mineral_fields }
    city = "xcd36a8"
    farm = "xa172c0"
    wood = "x916b9a"
    mine = "x826f33"
    arable_land = 298
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_silk_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_opium_plantations }
    capped_resources = {
        bg_iron_mining = 45
        bg_coal_mining = 80
        bg_logging = 11
    }
}

STATE_NORTH_GHANKEDHEN = {
    id = 403
    subsistence_building = "building_subsistence_farms"
    provinces = { "x222AD7" "x479430" "x95B7B5" "xA6AE3D" "xA9AB65" "xF25A26" }
    traits = { state_trait_kharunyana_river state_trait_ghankedhen_mineral_fields }
    city = "x479430"
    farm = "xa9ab65"
    wood = "xf25a26"
    mine = "x222ad7"
    arable_land = 199
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_opium_plantations }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 48
        bg_lead_mining = 19
        bg_logging = 9
    }
}

STATE_TUDHINA = {
    id = 404
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x489C7E" "x62A967" "xA3C3A5" "xB3BA79" }
    traits = { state_trait_marutha_desert }
    city = "xb3ba79"
    farm = "x62a967"
    mine = "x489c7e"
    arable_land = 55
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_sulfur_mining = 40
    }
}

STATE_PASIRAGHA = {
    id = 405
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x58C993" "x6302DC" "x7DBF42" "x88E800" "x989355" "xA9D442" "xBA9A7D" "xCBAD40" "xE7332B" }
    traits = { state_trait_dhenbasana_river state_trait_south_rahen_river_basin }
    city = "x58c993"
    farm = "x989355"
    mine = "xba9a7d"
    wood = "xcbad40"
    arable_land = 446
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_iron_mining = 24
        bg_logging = 7
    }
}

STATE_UPPER_DHENBASANA = {
    id = 406
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x183977" "x854117" "x8753C6" "xA6BA6A" "xB52626" "xCED26A" "xD4B22A" "xDF4CF9" }
    traits = { state_trait_dhenbasana_river state_trait_south_rahen_river_basin }
    city = "xd4b22a"
    farm = "x183977"
    mine = "xced26a"
    arable_land = 547
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_haless_spirits = 32
        bg_logging = 6
    }
}

STATE_LOWER_DHENBASANA = {
    id = 407
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x267B48" "x75EFA6" "x8DA76D" "x9A9200" "xB5E359" "xBAB34E" "xD02575" "xE39494" "xE73A35" }
    traits = { state_trait_dhenbasana_river state_trait_south_rahen_river_basin }
    city = "xd02575"
    farm = "xe39494"
    wood = "x9a9200"
    arable_land = 495
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_silk_plantations bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tea_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 9
    }
}

STATE_ASCENSION_JUNGLE = {
    id = 408
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x5B84CF" "x9958CF" "x9B995A" "xBA4A34" "xBA5A34" "xD5C226" "xDCBD8B" "xE58527" "xEA9C1E" }
    traits = { state_trait_harimari_jungles }
    city = "xdcbd8b"
    farm = "x5b84cf"
    wood = "xea9c1e"
    mine = "x9b995a"
    port = "xba5a34"
    arable_land = 105
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tea_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 26
        bg_iron_mining = 36
        bg_lead_mining = 45
        bg_fishing = 7
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 1
    }
    naval_exit_id = 3301 #Gulf of Rahen
}

STATE_TUJGAL = {
    id = 409
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x07B7DD" "x5A32DB" "xD48334" "xE36837" }
    traits = { state_trait_harimari_jungles state_trait_dhenbasana_river }
    city = "xe36837"
    farm = "xd48334"
    wood = "x5a32db"
    port = "x07b7dd"
    arable_land = 84
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tea_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 20
        bg_sulfur_mining = 36
        bg_fishing = 6
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 1
    }
    naval_exit_id = 3301 #Gulf of Rahen
}

STATE_BABHAGAMA = {
    id = 410
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1A9F3C" "x213002" "x7BD4D4" "xB2C192" "xDBCB47" }
    traits = { state_trait_kharunyana_river state_trait_marutha_desert }
    city = "xb2c192"
    farm = "x213002"
    mine = "xdbcb47"
    arable_land = 81
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_opium_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_coal_mining = 12
        bg_logging = 7
    }
}

STATE_SATARSAYA = {
    id = 411
    subsistence_building = "building_subsistence_farms"
    provinces = { "x29A44C" "x34A092" "x887C4D" "x8BD5A7" "x9D4888" "xA08BD5" "xA47B2C" "xB2BD6B" "xC65786" "xC8A1D2" }
    traits = {}
    city = "xb2bd6b"
    farm = "xa47b2c"
    arable_land = 195
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 4
    }
}

STATE_WEST_GHAVAANAJ = {
    id = 412
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x335811" "x6E9D67" "x718520" "x803A67" "x88682A" "x8C2231" "x9B405E" "xA0D04C" "xA7A32E" "xB33E2B" }
    traits = { state_trait_marutha_desert }
    city = "x803a67"
    farm = "x718520"
    arable_land = 165
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_lead_mining = 23
    }
}

STATE_EAST_GHAVAANAJ = {
    id = 413
    subsistence_building = "building_subsistence_farms"
    provinces = { "x213474" "x213530" "x421E74" "x766E15" "x7B502A" "x945115" "xABB113" "xAE1F62" "xAE245F" "xC13476" "xCD6132" "xFF6600" }
    traits = { state_trait_ghavaanaj_indigo_fields }
    city = "xc13476"
    farm = "x945115"
    wood = "x213474"
    arable_land = 342
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 14
    }
}

STATE_TUGHAYASA = {
    id = 414
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x247D83" "x31D0E0" "x546EF7" "x64537B" "xB1C8C9" "xB81B6C" }
    traits = { state_trait_kharunyana_river state_trait_tughayasa_mountain }
    city = "xb1c8c9"
    farm = "x247d83"
    mine = "x546ef7"
    wood = "x31d0e0"
    arable_land = 149
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_silk_plantations bg_cotton_plantations bg_dye_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 9
    }
}

STATE_DHUJAT = {
    id = 415
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x0D216F" "x1E1158" "x369163" "x4763C8" "x9A991B" "x9C74DD" "xE3AAA9" "xE69AD4" }
    prime_land = { "x9A991B" }
    traits = { state_trait_kharunyana_river state_trait_porcelain_cities state_trait_south_rahen_river_basin }
    city = "x9a991b"
    farm = "xe69ad4"
    wood = "x4763c8"
    arable_land = 473
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_silk_plantations bg_banana_plantations bg_cotton_plantations bg_dye_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 12
    }
}

STATE_SARISUNG = {
    id = 416
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x1A585C" "x30C77C" "x39BC98" "x4AA200" "x526600" "x664D59" "x9A3028" "xAB8461" "xECCB16" "xF60EE1" }
    traits = { state_trait_kharunyana_river state_trait_porcelain_cities }
    city = "x4aa200"
    farm = "x526600"
    wood = "x30c77c"
    arable_land = 417
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_silk_plantations bg_cotton_plantations bg_dye_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 11
        bg_coal_mining = 36
    }
}

STATE_TILTAGHAR = {
    id = 417
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x91783B" "xA92BCD" "xB9BA3C" "xE03798" }
    traits = { state_trait_marutha_desert }
    city = "xa92bcd"
    farm = "x91783b"
    arable_land = 31
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_opium_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 2
    }
}

STATE_WEST_NADIMRAJ = {
    id = 418
    subsistence_building = "building_subsistence_farms"
    provinces = { "x5383DC" "x5FD458" "x9F3C22" "xB1EC34" "xB55685" "xC98557" "xDB9040" }
    traits = { state_trait_kharunyana_river }
    city = "xb55685"
    farm = "x5383dc"
    mine = "xb1ec34"
    wood = "xdb9040"
    arable_land = 178
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_opium_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 8
    }
}

STATE_RAJNADHAGA = {
    id = 419
    subsistence_building = "building_subsistence_farms"
    provinces = { "x578D23" "x75D26B" "x767E48" "xAFD743" "xB8772A" "xD93A1F" "xD9F456" }
    traits = { state_trait_kharunyana_river }
    city = "xb8772a"
    farm = "x578d23"
    mine = "x75d26b"
    wood = "xd9f456"
    arable_land = 250
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_opium_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 8
        bg_iron_mining = 24
    }
}

STATE_CENTRAL_NADIMRAJ = {
    id = 420
    subsistence_building = "building_subsistence_farms"
    provinces = { "x183A9A" "x1BD435" "x563791" "x7B63D5" "x8BC46F" "x8F1CA4" "xCB1E8C" }
    traits = { state_trait_kharunyana_river }
    city = "x183a9a"
    farm = "xcb1e8c"
    mine = "x1bd435"
    arable_land = 218
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_opium_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 6
    }
}

STATE_EAST_NADIMRAJ = {
    id = 421
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x3D6614" "x4BC4C0" "x7C254E" "xC1A245" "xDB9D74" }
    traits = { state_trait_kharunyana_river }
    city = "xc1a245"
    farm = "x7c254e"
    arable_land = 165
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_dye_plantations bg_opium_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 5
    }
}

STATE_HOBGOBLIN_HOMELANDS = {
    id = 422
    subsistence_building = "building_subsistence_farms"
    provinces = { "x446C64" "x46BA6B" "x4FFC84" "x64E255" "xA64837" "xAB080F" "xB527C7" "xC893E7" }
    traits = { state_trait_kharunyana_falls state_trait_kharunyana_river state_trait_yanhe_river }
    city = "x46ba6b"
    farm = "xb527c7"
    mine = "xc893e7"
    arable_land = 238
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_opium_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 40
        bg_logging = 12
    }
}

STATE_GHILAKHAD = {
    id = 423
    subsistence_building = "building_subsistence_farms"
    provinces = { "x34A95A" "x3D917B" "x3F5D83" "x6C3D34" "xA74263" }
    traits = { state_trait_kharunyana_river }
    city = "x3f5d83"
    farm = "x3d917b"
    arable_land = 48
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_opium_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 3
    }
}

STATE_RAGHAJANDI = {
    id = 424
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x029FA4" "x03BE78" "x077301" "x38A045" "x7DAFF8" "x80B0FA" "x876160" "x948AF5" "xAEAD46" }
    traits = { state_trait_yanhe_river }
    city = "x80b0fa"
    farm = "x38a045"
    arable_land = 255
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_opium_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 3
        bg_sulfur_mining = 80
    }
}

STATE_GHATASAK = {
    id = 425
    subsistence_building = "building_subsistence_farms"
    provinces = { "x450EFA" "x79E130" "x9107A4" "xD10EE1" "xD8B799" "xF945FA" }
    traits = {}
    city = "x450efa"
    farm = "x79e130"
    arable_land = 81
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_opium_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 4
    }
}

STATE_SHAMAKHAD_PLAINS = {
    id = 426
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x32479D" "x4445C8" "x4DFBE9" "x5889E1" "x71C1CC" "x7F0EC8" "x985A07" "xB0B244" "xC00645" }
    prime_land = { "x71C1CC" "xB0B244" "x985A07" "x5889E1" }
    traits = { state_trait_yanhe_river }
    city = "x985a07"
    farm = "xc00645"
    wood = "x7f0ec8"
    arable_land = 372
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_cotton_plantations bg_opium_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 9
    }
}

STATE_SIR = {
    id = 427
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x080EAF" "x09B0E1" "x594596" "x8189AF" "xA80E96" "xA9B0C8" "xD045AF" "xF889C8" }
    traits = { state_trait_yanhe_river }
    city = "xf889c8"
    farm = "xa80e96"
    wood = "x080eaf"
    mine = "xa9b0c8"
    arable_land = 389
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_silk_plantations bg_opium_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 8
    }
}

STATE_SRAMAYA = {
    id = 428
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x4992E3" "x837BF6" "x913643" "xBA8887" "xD1CEE8" }
    prime_land = { "xBA8887" "xD1CEE8" "x837BF6" }
    traits = { state_trait_dhenbasana_river state_trait_kharunyana_river state_trait_great_rahen_estuaries state_trait_south_rahen_river_basin }
    city = "xba8887"
    farm = "x837bf6"
    wood = "x913643"
    port = "x4992e3"
    arable_land = 362
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_silk_plantations bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tea_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 12
        bg_fishing = 12
    }
    naval_exit_id = 3301 #Gulf of Rahen
}
