STATE_NIZVELS = {
    id = 248
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0AFDFD" "x26CBFD" "x2A7820" "x2D500A" "x46FDFD" "x565C7E" "x567442" "x7A0796" "xAA76AD" "xAB7A4F" "xB34059" }
    traits = {}
    city = "x2a7820" #Vels Domfan
    farm = "xab7a4f" #Stenurynn
    wood = "x565c7e" #Sardobnn
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_HRADAPOLERE = {
    id = 249
    subsistence_building = "building_subsistence_farms"
    provinces = { "x20FDFD" "x2B7C7E" "x2B8020" "x2C8820" "x8268E0" "xAB7EAD" "xAC8A4F" "xB77DB2" "xB938AE" "xC3285E" "xD54EAD" "xF3EBCE" }
    traits = {}
    city = "x2C8820" #Stanyrhrada
    farm = "xf3ebce" #Trompolere
    wood = "xb938ae" #Vels Amsto
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_ARVERYNN = {
    id = 250
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0A64FD" "x22FDFD" "x2C947E" "x57A6A9" "xAB824F" "xAB86AD" "xAD96AD" "xB8E085" "xCA8971" "xE89084" }
    traits = {}
    city = "xab824f" #Arverynn
    farm = "xab86ad" #Munamsto
    wood = "x0a64fd" #Vels Bacar
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_VIZANIRZAG = {
    id = 251
    subsistence_building = "building_subsistence_farms"
    provinces = { "x08DC28" "x09CACA" "x2C8C7E" "x2C9020" "x2D9820" "x2D9C7E" "x38CA10" "x4664FD" "x5550B3" "x7A3051" "x8AA90D" "x9BAA44" "xAC8EAD" "xAC924F" "xD14535" }
    traits = {}
    city = "x7a3051" #Virizerno
    farm = "x2c9020" #Orsiarinn
    wood = "x8aa90d" #Fadhevych
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_CORYNN = {
    id = 252
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0E3347" "x2B847E" "x2E2C7E" "x598A2B" "x8462B0" "x8953E4" "x8E97D7" "x93C6F9" "xA4C38B" "xAF324F" "xFB4936" }
    traits = {}
    city = "x598a2b" #Rohaves
    farm = "x8e97d7" #Poppyspring
    mine = "x2b847e" #Thromshana
    wood = "x0e3347" #Bedork
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_dye_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_iron_mining = 1
    }
}

STATE_ARNULYKAIR = {
    id = 253
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0ACAFC" "x1FC47C" "x2E247E" "x2E2820" "x30FDFD" "x6ACA62" "xAD224F" "xAE26AD" "xAEF6AD" "xD7220A" "xD7E30A" }
    traits = {}
    city = "xad224f" #Amstoynn
    farm = "xd7220a" #Tipneyshire
    wood = "xae26ad" #Velencestor
    mine = "xaef6ad" #Adirna
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_iron_mining = 1
        bg_lead_mining = 2
        bg_coal_mining = 1
    }
}

STATE_OSINDAIN = {
    id = 254
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2D2020" "x41362A" "x4A3479" "x4CAC90" "x572ADE" "x74035F" "x79BFE0" "x86C279" "xAD9A4F" "xAD9EAD" "xCF5860" "xE02C91" "xE4E0A7" }
    traits = {}
    city = "xad9a4f" #Vathres
    farm = "x86c279" #Szoholar
    mine = "x74035F" #Rezbainn
    wood = "x41362a" #Vyrgalykar
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_iron_mining = 3
        bg_lead_mining = 2
    }
}

STATE_IMSTYNN = {
    id = 255
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0000E8" "x09B4DF" "x1ACAFC" "x2E3020" "x304C7E" "x4078AC" "x6C8C8C" "xAE2A4F" "xC95F9C" "xCDE3A6" "xDC7A3B" }
    traits = {}
    city = "xae2a4f" #Neyholar
    farm = "x2e3020" #Talmsild
    wood = "x304c7e" #Sethcalrm
    mine = "xcde3a6" #Ujorfalon
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_coal_mining = 1
        bg_iron_mining = 3
        bg_lead_mining = 7
    }
}

STATE_NIZELYNN = {
    id = 256
    subsistence_building = "building_subsistence_farms"
    provinces = { "x30731E" "x4F7C7E" "x508020" "x77D750" "xB8B2B8" "xC815DF" "xCF76AD" "xCF7A4F" "xCF7EAD" "xD0824F" }
    traits = { state_trait_ynn_river }
    city = "x77d750" #Nizamsto
    farm = "xcf7a4f" #NEW PLACE
    mine = "xcf76ad" #Selocshana
    wood = "xcf7ead" #Kamhubi
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_iron_mining = 1
    }
}

STATE_EPADYNN = {
    id = 257
    subsistence_building = "building_subsistence_farms"
    provinces = { "x47FADC" "x4F747E" "x519020" "x839ACC" "x8CB45D" "xCF724F" }
    traits = { state_trait_ynn_river }
    city = "x4f747e" #Ynngard
    farm = "x839acc" #Vuhsbya
    wood = "xcf724f" #Epadarkan
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_MOKUNACH = {
    id = 258
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0100EC" "x239726" "x4C8AAE" "x4F7820" "x6A1AA0" "x6F282B" "xD18EAD" "xD1924F" "xDDD3D2" "xEABA36" }
    traits = {}
    city = "x4f7820" #Beggaston
    farm = "x6A1AA0" #NEW PLACE
    mine = "xd1924f" #NEW PLACE
    wood = "x6f282b" #Yanumpa
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_lead_mining = 1
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 1
        discover_amount_max = 1
    }
}

STATE_CIODSIBH = {
    id = 259
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0DCACA" "x21CFF3" "x2736FF" "x50D3E7" "x51947E" "x59D59D" "x73279D" "x823C32" "x8C7DA9" "x9D4E94" "xB407F5" "xD54A4F" "xD65EAD" "xEEF132" "xEFA010" }
    traits = {}
    city = "x9d4e94" #Plumstead
    farm = "x823C32" #Ubatap
    mine = "x51947E" #NEW PLACE
    wood = "x21cff3" #Cathfei
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 1
        discover_amount_max = 1
    }
}

STATE_BORULEED = {
    id = 260
    subsistence_building = "building_subsistence_farms"
    provinces = { "x02A7E0" "x0387F1" "x06CB06" "x2218A8" "x3123DD" "x397FCF" "x3F684C" "x43591B" "x4A00F8" "x565820" "x58F01D" "x5E37F5" "x81CD1A" "x8DFAB2" "x92226D" "x95955A" "x977065" "x9959FB" "x9A9361" "xA8029D" "xBFA411" "xCBEE81" "xCC2229" "xCC4EAD" "xCEA621" "xD373AF" "xD43A4F" "xD7C0DF" "xD882F4" }
    traits = {}
    city = "xa8029d" #Tiru Moine
    farm = "x3F684C" #NEW PLACE
    mine = "xd43a4f" #Esimoy
    wood = "x565820" #Tamwair
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_lead_mining = 4
        bg_iron_mining = 1
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 1
        discover_amount_max = 1
    }
}

STATE_MOYQUIN = {
    id = 261
    subsistence_building = "building_subsistence_farms"
    provinces = { "x01DEC2" "x15DC6A" "x32A3EF" "x543820" "x7852C7" "xAFD5DB" "xB64102" "xD336AD" "xD52C89" "xF639EE" }
    traits = {}
    city = "xf639ee" #NEW PLACE
    farm = "xb64102" #Auvutoi
    mine = "xd336ad" #Taslamo
    wood = "x15dc6a" #Tampu
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 1
        discover_amount_max = 1
    }
}

STATE_ORONTAS = {
    id = 262
    subsistence_building = "building_subsistence_farms"
    provinces = { "x03DD22" "x2EED5C" "x533020" "x53347E" "x67124D" "x7581D9" "xAFC854" "xD3324F" }
    traits = {}
    city = "x67124d" #NEW PLACE
    farm = "x53347e" #Sanatsiha
    mine = "xafc854" #Cianith
    wood = "x533020" #Orontas
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 1
        discover_amount_max = 1
    }
}

STATE_LEEDMOY = {
    id = 263
    subsistence_building = "building_subsistence_farms"
    provinces = { "x3A5AA8" "x492954" "x62B483" "xDA2BD3" }
    traits = {}
    city = "xDA2BD3" #NEW PLACE
    farm = "x3A5AA8" #NEW PLACE
    mine = "x492954" #NEW PLACE
    wood = "x62B483" #NEW PLACE
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 1
        discover_amount_max = 1
    }
}

STATE_PAIHUBI = {
    id = 264
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0DF97C" "x1BCCEF" "x522820" "x532C7E" "xA2F65A" "xD226AD" "xD32A4F" "xD32EAD" "xD90036" }
    traits = {}
    city = "xD32A4F" #Pelodaire
    farm = "xd32ead" #Jerarur
    mine = "x0DF97C" #Boruonn
    wood = "xD90036" #Bronmas
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 1
        discover_amount_max = 1
    }
}

STATE_VELS_FADHECAI = {
    id = 265
    subsistence_building = "building_subsistence_farms"
    provinces = { "x05C757" "x32A3E3" "x4E6C7E" "x4E7020" "x508C7E" "xCE6EAD" "xD08A4F" }
    traits = { state_trait_ynn_river }
    city = "x32a3e3" #Fadhecamsto
    farm = "xd08a4f" #Konyrynn
    mine = "x4e6c7e" #Konyrhrada
    wood = "x4e7020" #Fadheshana
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 2
        bg_iron_mining = 1
    }
}

STATE_KAZTAKRI = {
    id = 266
    subsistence_building = "building_subsistence_farms"
    provinces = { "x4E647E" "x50847E" "x508820" "x82ABE7" "xBBDCD7" "xCE66AD" "xD656AD" }
    traits = { state_trait_ynn_river }
    city = "x82abe7" #Minata
    farm = "xd656ad" #Adbraseloc
    wood = "xce66ad" #Epadobnn
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_KEHTAUK = {
    id = 267
    subsistence_building = "building_subsistence_farms"
    provinces = { "x226609" "x33DFFD" "x35CACA" "x373F8E" "x50FCFF" "x522020" "x52247E" "x91AB9A" "x92E3DF" "x9820A3" "xB11739" "xC227D9" "xD2224F" "xFC3DA5" "xFF664F" }
    traits = {}
    city = "xff664f" #Mara Luar
    farm = "x91ab9a" #Wunneauvu
    mine = "x33dffd" #Muyhenas
    wood = "x35caca" #Ekrnabit
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_lead_mining = 4
        bg_iron_mining = 1
    }
}

STATE_ARGANJUZORN = {
    id = 268
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2E999C" "x3FB373" "x4D6020" "x918823" "x96A842" "xC16892" "xCD624F" "xF78BB7" "xF7B2F5" }
    traits = { state_trait_ynn_river }
    city = "x918823" #Velikvab
    farm = "xcd624f" #Arganjuzorn
    mine = "x2e999c" #Ravata Vestor
    wood = "x4d6020" #NEW PLACE
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_coal_mining = 1
    }
}

STATE_TUKHNOSA = {
    id = 269
    subsistence_building = "building_subsistence_farms"
    provinces = { "x21AB8C" "x28DF24" "x4A06B0" "x4D5C7E" "x4E6820" "x84CCA4" "x91F5D1" "x9AD448" "xC54262" "xCA2941" "xCD5EAD" "xCE6A4F" "xD086AD" "xD19A4F" "xF01382" }
    traits = {}
    city = "x4e6820" #Ebenmas
    farm = "xcd5ead" #Tuhotyr
    mine = "xca2941" #Gosnahyr
    wood = "xc54262" #Ebymhubnn
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_coal_mining = 1
        bg_iron_mining = 1
        bg_lead_mining = 1
    }
}

STATE_KHIBERSERI = {
    id = 274
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0423EC" "x1620A9" "x1C63CA" "x4031B9" "x519820" "x529C7E" "x5EF03F" "x65C27F" "x6AC9DC" "x6FA266" "x73CD08" "x7C3A65" "x8113CC" "x92C193" "x92FD41" "x9B0DB2" "xB06090" "xB6510D" "xBCEC41" "xD19A99" "xD29EAD" "xDDE5A5" "xF508CE" }
    traits = {}
    city = "x8113CC" #NEW PLACE
    farm = "x519820" #Khiberseri
    mine = "x1C63CA" #Ciasuhul
    wood = "xD19A99" #NEW PLACE
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_lead_mining = 2
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 1
        discover_amount_max = 1
    }
}

STATE_ENASUMPA = {
    id = 275
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1DB8ED" "x376932" "x3C48C5" "x51565A" "x569559" "x5C5A32" "x6C5ABF" "x6FFD2D" "xBDED9A" "xC61D1A" "xCB0DB2" "xCB3A4F" "xD196AD" "xDE5B96" "xE0D7C4" "xE799E2" "xFB6977" }
    traits = {}
    city = "xc61d1a" #Varlenkrego
    farm = "xcb3a4f" #Drozmavarlena
    mine = "xcb0db2" #NEW PLACE
    wood = "xde5b96" #NEW PLACE
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_lead_mining = 1
        bg_iron_mining = 1
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 1
        discover_amount_max = 1
    }
}

STATE_POMVASONN = {
    id = 276
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0280B6" "x14FC80" "x3065B0" "x4CB25A" "x4D5820" "x73E3D7" "xCD5A4F" "xFD0582" "xFD2346" }
    traits = { state_trait_ynn_river }
    city = "x4D5820" #Pomvasonn
    farm = "x0280B6" #Drozmaynn
    wood = "x4cb25a" #Brancadynn
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_MACVISYE = {
    id = 277
    subsistence_building = "building_subsistence_farms"
    provinces = { "x326020" "x32647E" "x3E636E" "xB2624F" }
    traits = {}
    city = "x326020" #Drozma Mors
    farm = "xb2624f" #Solkaerh
    mine = "x32647E" #Macivisye
    wood = "x3e636e" #NEW PLACE
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_iron_mining = 1
    }
}

STATE_SVEMEL = {
    id = 278
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1ECBB0" "x326820" "x336C7E" "x44647E" "x635DF1" "x6B4507" "xB36A4F" "xEF695A" }
    traits = {}
    city = "xB36A4F" #Svemel
    farm = "x326820" #Ynnkmanja
    wood = "x44647E" #Gozhar Urthid
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_GOMOSENGHA = {
    id = 279
    subsistence_building = "building_subsistence_farms"
    provinces = { "x30E010" "x33D420" "x46747E" "x47847E" "x57B39E" "x8A5623" "xB36EAD" "xC7824F" "xC92A4F" "xDACD5C" "xFC0582" }
    traits = {}
    city = "x33d420" #Gomosengha
    farm = "x57B39E" #Velrekstepa
    mine = "x47847E" #Drozmagomod
    wood = "xb36ead" #Sengha Urthid
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_iron_mining = 1
    }
}

STATE_DROZMALREVO = {
    id = 280
    subsistence_building = "building_subsistence_farms"
    provinces = { "x457020" "x467820" "x8C10F7" "xA5B2DA" "xC566AD" "xC56EAD" "xC5724F" "xC676AD" "xE08DF2" }
    traits = {}
    city = "xc676ad" #Revmejan
    farm = "xc5724f" #Random
    wood = "xc566ad" #Random
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_DOLENYNN = {
    id = 281
    subsistence_building = "building_subsistence_farms"
    provinces = { "x446020" "xA9B0FB" "xB2CAAD" "xB451D6" "xC45EAD" "xC4624F" }
    traits = {}
    city = "xB451D6" #Ladjetrevo
    farm = "xB2CAAD" #Ynn Urthid
    wood = "x446020" #Lisicalrevo
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_iron_mining = 1
    }
}

STATE_GRUMLOKA = {
    id = 282
    subsistence_building = "building_subsistence_farms"
    provinces = { "x304020" "x315820" "x3677CF" }
    traits = {}
    city = "x315820" #Grumloka
    farm = "x3677cf" #NEW PLACE
    wood = "x304020" #Trevovasonn
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_MALACYNN = {
    id = 283
    subsistence_building = "building_subsistence_farms"
    provinces = { "xaf3ead" "xced94d" "xd4cba5" }
    traits = {}
    city = "xaf3ead" #Bojev Stara
    farm = "xced94d" #Atrad Tailok
    wood = "xd4cba5" #Revomors
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_DREVKENUC = {
    id = 284
    subsistence_building = "building_subsistence_farms"
    provinces = { "x27BD4B" "x2FEC20" "x445DAE" "x497957" "x76A4B1" "xAF3A4F" "xC47ACB" "xDE18A2" "xE4FCDC" }
    traits = {}
    city = "x445dae" #Alrev Urthid
    farm = "x2fec20" #Grebshalas
    mine = "x27bd4b" #Dregyljeim
    wood = "xc47acb" #Odmorovca
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 2
        bg_coal_mining = 1
    }
}

STATE_ROGAVYNN = {
    id = 285
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1C4DD9" "x2238F2" "x42E16E" "xAF36AD" "xB0424F" "xC066EA" "xC9CE36" "xF4A0DA" }
    traits = {}
    city = "xaf36ad" #Bosancovac
    farm = "x42e16e" #Stantirshalas
    mine = "x1c4dd9" #Morsynn
    wood = "xc066ea" #Nuv Dkandrozma
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_iron_mining = 1
    }
}

STATE_HRILAR = {
    id = 286
    subsistence_building = "building_subsistence_farms"
    provinces = { "x029AC8" "x0433F6" "x43547E" "x90F2A4" "x911741" "x996A84" "xCB314E" "xFC375D" }
    traits = {}
    city = "x0433f6" #Hrilar
    farm = "xcb314e" #Gozhar Gomod
    wood = "x43547e" #Shwarnadh
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_VERZEL = {
    id = 287
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1B8D91" "x445C7E" "x456820" "xA78819" }
    traits = {}
    city = "x456820" #Random
    farm = "xa78819" #Random
    wood = "x1b8d91" #Random
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_VITREYNN = {
    id = 288
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2F347E" "x30447E" "x31547E" "x5D6B95" "x68D1F9" "x8C193E" "xA24080" "xB0E010" "xB156AD" "xDFEC70" }
    traits = {}
    city = "x30447E" #Amacimst
    farm = "x31547E" #Odmoreoch
    wood = "xB156AD" #Adkyanzabr
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_VARBUKLAND = {
    id = 289
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2BBF2E" "x30800E" "x30AC20" "x315020" "x553B3F" "x592AB4" "x8830BD" "xB04A4F" "xB14EAD" "xC7DD44" "xE7F68A" }
    traits = {}
    city = "xb04a4f" #Random
    farm = "x30ac20" #Random
    wood = "x315020" #Random
    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_KEWDHEMR = {
    id = 290
    subsistence_building = "building_subsistence_farms"
    provinces = { "x02C052" "x144683" "x325C7E" "x33747E" "x347820" "x3B77C3" "x45155F" "x4A805F" "x4CF080" "x521B0A" "x5888F2" "x90C0D0" "x9A65E7" "xA86344" "xB1524F" "xB25EAD" "xB376AD" "xB6AC81" "xC42462" "xC896AD" "xCC30AA" "xEFB409" }
    traits = {}
    city = "xb1524f" #Random
    farm = "x325c7e" #Random
    wood = "x9a65e7" #Random
    arable_land = 16
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_ZEMSHOGEN = {
    id = 294
    subsistence_building = "building_subsistence_farms"
    provinces = { "x01BF3A" "x234A11" "x350B0F" "x435020" "x47BA2C" "x7F712C" "x8A9598" "x8E85EB" "x94AA52" "xB15A4F" "xBE17B4" "xD88028" "xDAC04E" }
    traits = {}
    city = "x94aa52" #Random
    farm = "x8a9598" #Random
    wood = "x47ba2c" #Random
    arable_land = 19
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_WHETJUL = {
    id = 291
    subsistence_building = "building_subsistence_farms"
    provinces = { "x26952C" "x3618F8" "x37EB2E" "x424820" "x5BA7C2" "x774C99" "x8E9293" "x9EF738" "xB0E4AA" "xB3A09E" "xB47A4F" "xB79F3F" "xC246AD" "xC544F3" "xD2DD34" "xD2FCCD" "xF063FA" }
    traits = {}
    city = "xc544f3" #Random
    farm = "xb47a4f" #Random
    wood = "x424820" #Random
    arable_land = 18
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_NEFRNMEHN = {
    id = 292
    subsistence_building = "building_subsistence_farms"
    provinces = { "x10C0D0" "x1205C7" "x12C24F" "x1BC85D" "x42447E" "x5F5474" "x62F8C0" "x706090" "x8AA282" "x972B02" "xA26430" "xA44376" "xAB76FA" "xB180F2" "xC34A4F" "xC34EAD" "xCF5848" "xEC5162" }
    traits = {}
    city = "xc34a4f" #Random
    farm = "x8aa282" #Random
    wood = "xa44376" #Random
    arable_land = 14
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}

STATE_HEHADEDPAR = {
    id = 293
    subsistence_building = "building_subsistence_farms"
    provinces = { "x3AB420" "x435071" "x438067" "x4BFE37" "x5A7430" "x5B6292" "x9C5DD8" "xA427EB" "xB04111" "xB41CB3" "xB9424F" "xBC83B9" "xF1F9BA" "xFA990A" }
    traits = {}
    city = "x3ab420" #Random
    farm = "x435071" #Random
    wood = "xb04111" #Random
    arable_land = 15
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}
