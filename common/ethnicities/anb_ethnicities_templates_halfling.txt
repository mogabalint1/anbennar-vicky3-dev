﻿# Receives data from /genes
# Ethnicity templates are used in other ethnicities and templates in this folder

halfling = {

	@neg1_min = 0.4
	@neg1_max = 0.5

	@neg2_min = 0.3
	@neg2_max = 0.4

	@neg3_min = 0.1
	@neg3_max = 0.3

	@pos1_min = 0.5
	@pos1_max = 0.6

	@pos2_min = 0.6
	@pos2_max = 0.7

	@pos3_min = 0.7
	@pos3_max = 0.9

	@beauty1min = 0.35
	@beauty1max = 0.65

	@beauty2min = 0.4
	@beauty2max = 0.6

	@beauty3min = 0.45
	@beauty3max = 0.55

	@blend1min = 0.0
	@blend1max = 0.2

	@blend2min = 0.2
	@blend2max = 0.5

	@blend3min = 0.5
	@blend3max = 0.8

	skin_color = {
		10 = { 0.0 0.0 0.5 0.12 }
	}
	eye_color = {
		# Blue
		10 = { 0.65 0.05 1.0 0.6 }
		# Green
		10 = { 0.5 0.05 0.65 0.6 }
		# Brown
		10 = { 0.1 0.2 0.4 0.8 }
	}
	hair_color = {
		# Blonde
		10 = { 0.4 0.25 0.75 0.5 }
		# Brown 
		50 = { 0.65 0.5 0.9 0.9 }   #mostly brown haired
		# Red
		10 = { 0.85 0.0 0.9 0.5 }
		# Black
		20 = { 0.0 0.9 0.2 0.9 }
	}


	### Additive anim genes ###
    gene_chin_forward = {


        20 = { name = chin_forward   range = { 0.3 0.45 }      }
        64 = { name = chin_forward   range = { 0.45 0.55 }      }
        7 = { name = chin_forward   range = { 0.55 0.7 }      }


    }

    gene_chin_height = {

        20 = { name = chin_height   range = { 0.4 0.45 }      }
        84 = { name = chin_height   range = { 0.45 0.55 }      }
        7 = { name = chin_height   range = { 0.55 0.7 }      }


    }

    gene_chin_width = {


        17 = { name = chin_width   range = { 0.3 0.45 }      }
        64 = { name = chin_width   range = { 0.45 0.55 }      }
        17 = { name = chin_width   range = { 0.55 0.7 }      }


    }

    gene_eye_angle = {

        7 = { name = eye_angle   range = { 0.3 0.45 }      }
        84 = { name = eye_angle   range = { 0.45 0.55 }      }
        20 = { name = eye_angle   range = { 0.55 0.7 }      }

    }

    gene_eye_depth = {


        17 = { name = eye_depth   range = { 0.3 0.45 }      }
        64 = { name = eye_depth   range = { 0.45 0.55 }      }
        17 = { name = eye_depth   range = { 0.55 0.7 }      }


    }

    gene_eye_distance = {


        12 = { name = eye_distance   range = { 0.3 0.45 }      }
        74 = { name = eye_distance   range = { 0.45 0.55 }      }
        12 = { name = eye_distance   range = { 0.55 0.6 }      }


    }

    gene_eye_height = {


        17 = { name = eye_height   range = { 0.35 0.45 }      }
        64 = { name = eye_height   range = { 0.45 0.55 }      }
        7 = { name = eye_height   range = { 0.55 0.6 }      }

    }

    gene_eye_shut = {


        30 = { name = eye_shut   range = { 0.3 0.45 }      }
        84 = { name = eye_shut   range = { 0.45 0.55 }      }
        7 = { name = eye_shut   range = { 0.55 0.7 }      }


    }

    gene_forehead_angle = {


        17 = { name = forehead_angle   range = { 0.3 0.45 }      }
        64 = { name = forehead_angle   range = { 0.45 0.55 }      }
        17 = { name = forehead_angle   range = { 0.55 0.7 }      }


    }

    gene_forehead_brow_height = {


        17 = { name = forehead_brow_height   range = { 0.3 0.45 }      }
        64 = { name = forehead_brow_height   range = { 0.45 0.55 }      }
        27 = { name = forehead_brow_height   range = { 0.55 0.7 }      }


    }

    gene_forehead_height = {

        1 = { name = forehead_height   range = { 0.1 0.2 }      }
        17 = { name = forehead_height   range = { 0.2 0.45 }      }
        64 = { name = forehead_height   range = { 0.45 0.55 }      }
        17 = { name = forehead_height   range = { 0.55 0.8 }      }
        1 = { name = forehead_height   range = { 0.8 0.9 }      }

    }

    gene_forehead_roundness = {

        1 = { name = forehead_roundness   range = { 0.1 0.2 }      }
        17 = { name = forehead_roundness   range = { 0.2 0.45 }      }
        64 = { name = forehead_roundness   range = { 0.45 0.55 }      }
        17 = { name = forehead_roundness   range = { 0.55 0.8 }      }
        1 = { name = forehead_roundness   range = { 0.8 0.9 }      }

    }

    gene_forehead_width = {

        64 = { name = forehead_width   range = { 0.45 0.55 }      }     #wider forehead in general for more heart shaped face
        27 = { name = forehead_width   range = { 0.55 0.8 }      }
        1 = { name = forehead_width   range = { 0.8 0.9 }      }

    }

    gene_head_height = {

        7 = { name = head_height   range = { 0.35 0.45 }      }
        84 = { name = head_height   range = { 0.45 0.55 }      }

    }

    gene_head_profile = {

        1 = { name = head_profile   range = { 0.1 0.2 }      }
        17 = { name = head_profile   range = { 0.2 0.45 }      }
        64 = { name = head_profile   range = { 0.45 0.55 }      }
        17 = { name = head_profile   range = { 0.55 0.8 }      }
        1 = { name = head_profile   range = { 0.8 0.9 }      }

    }

    gene_head_top_height = {

        1 = { name = head_top_height   range = { 0.1 0.2 }      }
        27 = { name = head_top_height   range = { 0.2 0.45 }      }     #higher chance of this as more height is egghead look and this aint that
        84 = { name = head_top_height   range = { 0.45 0.55 }      }

    }

    gene_head_top_width = {

        84 = { name = head_top_width   range = { 0.45 0.55 }      }     #wider head top for heart shape
        27 = { name = head_top_width   range = { 0.55 0.8 }      }
        1 = { name = head_top_width   range = { 0.8 0.9 }      }

    }

    gene_head_width = {

        64 = { name = head_width   range = { 0.45 0.55 }      }
        17 = { name = head_width   range = { 0.55 0.8 }      }
        1 = { name = head_width   range = { 0.8 0.9 }      }

    }

    gene_jaw_angle = {

        1 = { name = jaw_angle   range = { 0.1 0.2 }      }
        37 = { name = jaw_angle   range = { 0.2 0.45 }      }   #definitely less sharp
        64 = { name = jaw_angle   range = { 0.45 0.55 }      }
        7 = { name = jaw_angle   range = { 0.55 0.8 }      }

    }

    gene_jaw_forward = {


        7 = { name = jaw_forward   range = { 0.3 0.45 }      }
        84 = { name = jaw_forward   range = { 0.45 0.55 }      }
        7 = { name = jaw_forward   range = { 0.55 0.7 }      }


    }

    gene_jaw_height = {


        7 = { name = jaw_height   range = { 0.3 0.45 }      }
        84 = { name = jaw_height   range = { 0.45 0.6 }      }  #slightly higher treshold
        7 = { name = jaw_height   range = { 0.6 0.7 }      }


    }

    gene_jaw_width = {

        1 = { name = jaw_width   range = { 0.1 0.2 }      }
        7 = { name = jaw_width   range = { 0.2 0.45 }      }
        64 = { name = jaw_width   range = { 0.45 0.55 }      }
        17 = { name = jaw_width   range = { 0.55 0.8 }      }   #slightly higher chance of wider jaw
        1 = { name = jaw_width   range = { 0.8 0.9 }      }

    }

    gene_mouth_corner_height = {


        12 = { name = mouth_corner_height   range = { 0.3 0.45 }      }
        74 = { name = mouth_corner_height   range = { 0.45 0.55 }      }
        40 = { name = mouth_corner_height   range = { 0.55 0.8 }      }     #happier!


    }

    gene_mouth_forward = {


        7 = { name = mouth_forward   range = { 0.3 0.45 }      }
        84 = { name = mouth_forward   range = { 0.45 0.55 }      }
        7 = { name = mouth_forward   range = { 0.55 0.6 }      }    #0.7 to 0.6
  

    }

    gene_mouth_height = {


        17 = { name = mouth_height   range = { 0.3 0.45 }      }
        64 = { name = mouth_height   range = { 0.45 0.55 }      }
        37 = { name = mouth_height   range = { 0.55 0.7 }      }    #higher chance of more squished face


    }

    gene_mouth_lower_lip_size = {

        7 = { name = mouth_lower_lip_size   range = { 0.2 0.45 }      }
        84 = { name = mouth_lower_lip_size   range = { 0.45 0.55 }      }
        7 = { name = mouth_lower_lip_size   range = { 0.55 0.7 }      }

    }

    gene_mouth_open = {

        7 = { name = mouth_open   range = { 0.2 0.45 }      }
        84 = { name = mouth_open   range = { 0.45 0.55 }      }
        7 = { name = mouth_open   range = { 0.55 0.8 }      }
        1 = { name = mouth_open   range = { 0.8 0.9 }      }

    }

    gene_mouth_upper_lip_size = {

 
        37 = { name = mouth_upper_lip_size   range = { 0.2 0.45 }      }    #higher chance of this
        84 = { name = mouth_upper_lip_size   range = { 0.45 0.55 }      }
        7 = { name = mouth_upper_lip_size   range = { 0.55 0.8 }      }
        1 = { name = mouth_upper_lip_size   range = { 0.8 0.9 }      }

    }

    gene_mouth_width = {

        64 = { name = mouth_width   range = { 0.45 0.55 }      }
        27 = { name = mouth_width   range = { 0.55 0.8 }      }     #wider mouths
        1 = { name = mouth_width   range = { 0.8 0.9 }      }

    }

    gene_neck_length = {


        17 = { name = neck_length   range = { 0.3 0.45 }      }
        64 = { name = neck_length   range = { 0.45 0.55 }      }
        17 = { name = neck_length   range = { 0.55 0.6 }      }


    }

    gene_neck_width = {


        17 = { name = neck_width   range = { 0.4 0.45 }      }
        64 = { name = neck_width   range = { 0.45 0.55 }      }
        17 = { name = neck_width   range = { 0.55 0.6 }      }


    }

    ### Blend shape genes ###
    gene_cheek_fat = {


        17 = { name = cheek_fat   range = { 0.4 0.55 }      }
        64 = { name = cheek_fat   range = { 0.55 0.7 }      }
        27 = { name = cheek_fat   range = { 0.7 0.9 }      }   #fuller cheeks in general


    }

    gene_cheek_forward = {


        7 = { name = cheek_forward   range = { 0.4 0.5 }      }
        84 = { name = cheek_forward   range = { 0.5 0.6 }      }
        17 = { name = cheek_forward   range = { 0.6 0.8 }      } #as above


    }

    gene_cheek_height = {


        7 = { name = cheek_height   range = { 0.3 0.45 }      }
        84 = { name = cheek_height   range = { 0.45 0.55 }      }
        7 = { name = cheek_height   range = { 0.55 0.7 }      }


    }

    gene_cheek_prom = {


        7 = { name = cheek_prom   range = { 0.4 0.45 }      }       #no prominence in cheeks really
        84 = { name = cheek_prom   range = { 0.45 0.55 }      }


    }

    gene_cheek_width = {


        7 = { name = cheek_width   range = { 0.45 0.55 }      }
        84 = { name = cheek_width   range = { 0.55 0.7 }      }        #wiide cheeks
        7 = { name = cheek_width   range = { 0.7 0.9 }      }


    }

    gene_ear_angle = {

        1 = { name = ear_angle   range = { 0.1 0.2 }      }
        17 = { name = ear_angle   range = { 0.2 0.45 }      }
        64 = { name = ear_angle   range = { 0.45 0.55 }      }
        17 = { name = ear_angle   range = { 0.55 0.8 }      }
        1 = { name = ear_angle   range = { 0.8 0.9 }      }

    }

    gene_ear_inner_shape = {

        1 = { name = ear_inner_shape   range = { 0.1 0.2 }      }
        17 = { name = ear_inner_shape   range = { 0.2 0.45 }      }
        64 = { name = ear_inner_shape   range = { 0.45 0.55 }      }
        17 = { name = ear_inner_shape   range = { 0.55 0.8 }      }
        1 = { name = ear_inner_shape   range = { 0.8 0.9 }      }

    }

    gene_ear_lower_bend = {

        64 = { name = ear_lower_bend   range = { 0.45 0.55 }      }
        17 = { name = ear_lower_bend   range = { 0.55 0.8 }      }  #sticky out usually
        1 = { name = ear_lower_bend   range = { 0.8 0.9 }      }

    }

    gene_ear_out = {


        64 = { name = ear_out   range = { 0.6 0.7 }      }    #ear outs
        27 = { name = ear_out   range = { 0.7 0.8 }      }
        1 = { name = ear_out   range = { 0.8 0.9 }      }

    }

    gene_ear_size = {

        84 = { name = ear_size   range = { 0.45 0.55 }      }
        27 = { name = ear_size   range = { 0.55 0.8 }      }     #bigger end
        1 = { name = ear_size   range = { 0.8 0.9 }      }

    }

    gene_ear_upper_bend = {


        44 = { name = ear_upper_bend   range = { 0.5 0.6 }      }     #bendy
        47 = { name = ear_upper_bend   range = { 0.6 0.8 }      }
        1 = { name = ear_upper_bend   range = { 0.8 0.9 }      }

    }

    gene_eye_corner_def = {

        1 = { name = eye_corner_def   range = { 0.1 0.2 }      }
        7 = { name = eye_corner_def   range = { 0.2 0.45 }      }
        84 = { name = eye_corner_def   range = { 0.45 0.55 }      }
        7 = { name = eye_corner_def   range = { 0.55 0.8 }      }
        1 = { name = eye_corner_def   range = { 0.8 0.9 }      }

    }

    gene_eye_corner_depth_min = {

        1 = { name = eye_corner_depth_min   range = { 0.1 0.2 }      }
        17 = { name = eye_corner_depth_min   range = { 0.2 0.45 }      }
        64 = { name = eye_corner_depth_min   range = { 0.45 0.55 }      }
        17 = { name = eye_corner_depth_min   range = { 0.55 0.8 }      }
        1 = { name = eye_corner_depth_min   range = { 0.8 0.9 }      }

    }

    gene_eye_fold_droop = {

        1 = { name = eye_fold_droop   range = { 0.1 0.2 }      }
        17 = { name = eye_fold_droop   range = { 0.2 0.45 }      }
        64 = { name = eye_fold_droop   range = { 0.45 0.55 }      }
        17 = { name = eye_fold_droop   range = { 0.55 0.8 }      }
        1 = { name = eye_fold_droop   range = { 0.8 0.9 }      }

    }

    gene_eye_fold_shape = {

        1 = { name = eye_fold_shape   range = { 0.1 0.2 }      }
        17 = { name = eye_fold_shape   range = { 0.2 0.45 }      }
        64 = { name = eye_fold_shape   range = { 0.45 0.55 }      }
        17 = { name = eye_fold_shape   range = { 0.55 0.8 }      }
        1 = { name = eye_fold_shape   range = { 0.8 0.9 }      }

    }

    gene_eye_size = {


        17 = { name = eye_size   range = { 0.3 0.45 }      }
        64 = { name = eye_size   range = { 0.45 0.55 }      }
        17 = { name = eye_size   range = { 0.55 0.6 }      }


    }

    gene_eye_upper_lid_size = {

        1 = { name = eye_upper_lid_size   range = { 0.1 0.2 }      }
        17 = { name = eye_upper_lid_size   range = { 0.2 0.45 }      }
        64 = { name = eye_upper_lid_size   range = { 0.45 0.55 }      }
        17 = { name = eye_upper_lid_size   range = { 0.55 0.8 }      }
        1 = { name = eye_upper_lid_size   range = { 0.8 0.9 }      }

    }

    gene_forehead_brow_curve = {

        1 = { name = forehead_brow_curve   range = { 0.1 0.2 }      }
        17 = { name = forehead_brow_curve   range = { 0.2 0.45 }      }
        64 = { name = forehead_brow_curve   range = { 0.45 0.55 }      }
        17 = { name = forehead_brow_curve   range = { 0.55 0.8 }      }
        1 = { name = forehead_brow_curve   range = { 0.8 0.9 }      }

    }

    gene_forehead_brow_forward = {

        1 = { name = forehead_brow_forward   range = { 0.1 0.2 }      }
        7 = { name = forehead_brow_forward   range = { 0.2 0.45 }      }
        84 = { name = forehead_brow_forward   range = { 0.45 0.55 }      }
        7 = { name = forehead_brow_forward   range = { 0.55 0.8 }      }
        1 = { name = forehead_brow_forward   range = { 0.8 0.9 }      }

    }

    gene_forehead_brow_inner_height = {

        1 = { name = forehead_brow_inner_height   range = { 0.1 0.2 }      }
        7 = { name = forehead_brow_inner_height   range = { 0.2 0.45 }      }
        84 = { name = forehead_brow_inner_height   range = { 0.45 0.55 }      }
        37 = { name = forehead_brow_inner_height   range = { 0.55 0.8 }      }  #sadder look lol
        1 = { name = forehead_brow_inner_height   range = { 0.8 0.9 }      }

    }

    gene_forehead_brow_outer_height = {

        1 = { name = forehead_brow_outer_height   range = { 0.1 0.2 }      }
        27 = { name = forehead_brow_outer_height   range = { 0.2 0.45 }      }
        84 = { name = forehead_brow_outer_height   range = { 0.45 0.55 }      }
        27 = { name = forehead_brow_outer_height   range = { 0.55 0.8 }      }      #halfling can do a mean brow
        1 = { name = forehead_brow_outer_height   range = { 0.8 0.9 }      }

    }

    gene_forehead_brow_width = {

        1 = { name = forehead_brow_width   range = { 0.1 0.2 }      }
        7 = { name = forehead_brow_width   range = { 0.2 0.45 }      }
        84 = { name = forehead_brow_width   range = { 0.45 0.55 }      }
        7 = { name = forehead_brow_width   range = { 0.55 0.8 }      }
        1 = { name = forehead_brow_width   range = { 0.8 0.9 }      }

    }

    gene_forehead_roundness = {

        1 = { name = forehead_roundness   range = { 0.1 0.2 }      }
        17 = { name = forehead_roundness   range = { 0.2 0.45 }      }
        64 = { name = forehead_roundness   range = { 0.45 0.55 }      }
        17 = { name = forehead_roundness   range = { 0.55 0.8 }      }
        1 = { name = forehead_roundness   range = { 0.8 0.9 }      }

    }

    gene_jaw_def = {

        10 = { name = jaw_def   range = { 0.1 0.2 }      }
        57 = { name = jaw_def   range = { 0.2 0.45 }      } 
        50 = { name = jaw_def   range = { 0.45 0.55 }      }    #in general lower def
        7 = { name = jaw_def   range = { 0.55 0.8 }      }

    }

    gene_mouth_lower_lip_def = {

        1 = { name = mouth_lower_lip_def   range = { 0.1 0.2 }      }
        17 = { name = mouth_lower_lip_def   range = { 0.2 0.45 }      }
        64 = { name = mouth_lower_lip_def   range = { 0.45 0.55 }      }
        17 = { name = mouth_lower_lip_def   range = { 0.55 0.8 }      }
        1 = { name = mouth_lower_lip_def   range = { 0.8 0.9 }      }

    }

    gene_mouth_lower_lip_full = {

        1 = { name = mouth_lower_lip_full   range = { 0.1 0.2 }      }
        17 = { name = mouth_lower_lip_full   range = { 0.2 0.45 }      }
        64 = { name = mouth_lower_lip_full   range = { 0.45 0.55 }      }
        17 = { name = mouth_lower_lip_full   range = { 0.55 0.8 }      }
        1 = { name = mouth_lower_lip_full   range = { 0.8 0.9 }      }

    }

    gene_mouth_lower_lip_pads = {

        1 = { name = mouth_lower_lip_pads   range = { 0.1 0.2 }      }
        17 = { name = mouth_lower_lip_pads   range = { 0.2 0.45 }      }
        64 = { name = mouth_lower_lip_pads   range = { 0.45 0.55 }      }
        17 = { name = mouth_lower_lip_pads   range = { 0.55 0.8 }      }
        1 = { name = mouth_lower_lip_pads   range = { 0.8 0.9 }      }

    }

    gene_mouth_lower_lip_width = {

        1 = { name = mouth_lower_lip_width   range = { 0.1 0.2 }      }
        7 = { name = mouth_lower_lip_width   range = { 0.2 0.45 }      }
        84 = { name = mouth_lower_lip_width   range = { 0.45 0.55 }      }
        7 = { name = mouth_lower_lip_width   range = { 0.55 0.8 }      }
        1 = { name = mouth_lower_lip_width   range = { 0.8 0.9 }      }

    }

    gene_mouth_philtrum_curve = {

        1 = { name = mouth_philtrum_curve   range = { 0.1 0.2 }      }
        17 = { name = mouth_philtrum_curve   range = { 0.2 0.45 }      }
        64 = { name = mouth_philtrum_curve   range = { 0.45 0.55 }      }
        17 = { name = mouth_philtrum_curve   range = { 0.55 0.8 }      }
        1 = { name = mouth_philtrum_curve   range = { 0.8 0.9 }      }

    }

    gene_mouth_philtrum_def = {

        1 = { name = mouth_philtrum_def   range = { 0.1 0.2 }      }
        17 = { name = mouth_philtrum_def   range = { 0.2 0.45 }      }
        64 = { name = mouth_philtrum_def   range = { 0.45 0.55 }      }
        17 = { name = mouth_philtrum_def   range = { 0.55 0.8 }      }
        1 = { name = mouth_philtrum_def   range = { 0.8 0.9 }      }

    }

    gene_mouth_philtrum_width = {

        1 = { name = mouth_philtrum_width   range = { 0.1 0.2 }      }
        7 = { name = mouth_philtrum_width   range = { 0.2 0.45 }      }
        84 = { name = mouth_philtrum_width   range = { 0.45 0.55 }      }
        7 = { name = mouth_philtrum_width   range = { 0.55 0.8 }      }
        1 = { name = mouth_philtrum_width   range = { 0.8 0.9 }      }

    }

    gene_mouth_upper_lip_curve = {

        1 = { name = mouth_upper_lip_curve   range = { 0.1 0.2 }      }
        17 = { name = mouth_upper_lip_curve   range = { 0.2 0.45 }      }
        64 = { name = mouth_upper_lip_curve   range = { 0.45 0.55 }      }
        17 = { name = mouth_upper_lip_curve   range = { 0.55 0.8 }      }
        1 = { name = mouth_upper_lip_curve   range = { 0.8 0.9 }      }

    }

    gene_mouth_upper_lip_def = {

        1 = { name = mouth_upper_lip_def   range = { 0.1 0.2 }      }
        17 = { name = mouth_upper_lip_def   range = { 0.2 0.45 }      }
        64 = { name = mouth_upper_lip_def   range = { 0.45 0.55 }      }
        17 = { name = mouth_upper_lip_def   range = { 0.55 0.8 }      }
        1 = { name = mouth_upper_lip_def   range = { 0.8 0.9 }      }

    }

    gene_mouth_upper_lip_full = {

        1 = { name = mouth_upper_lip_full   range = { 0.1 0.2 }      }
        17 = { name = mouth_upper_lip_full   range = { 0.2 0.45 }      }
        64 = { name = mouth_upper_lip_full   range = { 0.45 0.55 }      }
        17 = { name = mouth_upper_lip_full   range = { 0.55 0.8 }      }
        1 = { name = mouth_upper_lip_full   range = { 0.8 0.9 }      }

    }

    gene_mouth_upper_lip_width = {

        1 = { name = mouth_upper_lip_width   range = { 0.1 0.2 }      }
        7 = { name = mouth_upper_lip_width   range = { 0.2 0.45 }      }
        84 = { name = mouth_upper_lip_width   range = { 0.45 0.55 }      }
        7 = { name = mouth_upper_lip_width   range = { 0.55 0.8 }      }
        1 = { name = mouth_upper_lip_width   range = { 0.8 0.9 }      }

    }

    gene_nose_curve = {

        1 = { name = nose_curve   range = { 0.1 0.2 }      }
        12 = { name = nose_curve   range = { 0.2 0.45 }      }
        74 = { name = nose_curve   range = { 0.45 0.55 }      }
        12 = { name = nose_curve   range = { 0.55 0.8 }      }
        1 = { name = nose_curve   range = { 0.8 0.9 }      }

    }

    gene_nose_forward = {

        1 = { name = nose_forward   range = { 0.1 0.2 }      }
        12 = { name = nose_forward   range = { 0.2 0.45 }      }
        74 = { name = nose_forward   range = { 0.45 0.55 }      }
        12 = { name = nose_forward   range = { 0.55 0.8 }      }
        1 = { name = nose_forward   range = { 0.8 0.9 }      }

    }

    gene_nose_hawk = {

        1 = { name = nose_hawk   range = { 0.1 0.2 }      }
        37 = { name = nose_hawk   range = { 0.2 0.45 }      }
        84 = { name = nose_hawk   range = { 0.45 0.55 }      }      #they dont do hawk noses

    }

    gene_nose_height = {


        64 = { name = nose_height   range = { 0.45 0.55 }      }    #higher
        37 = { name = nose_height   range = { 0.55 0.7 }      }


    }

    gene_nose_length = {

        84 = { name = nose_length   range = { 0.45 0.55 }      }
        27 = { name = nose_length   range = { 0.55 0.7 }      }     #can be bigger longer nose


    }

    gene_nose_nostril_angle = {

        1 = { name = nose_nostril_angle   range = { 0.1 0.2 }      }
        7 = { name = nose_nostril_angle   range = { 0.2 0.45 }      }
        84 = { name = nose_nostril_angle   range = { 0.45 0.55 }      }
        27 = { name = nose_nostril_angle   range = { 0.55 0.8 }      }  #more chance for nosey nose
        1 = { name = nose_nostril_angle   range = { 0.8 0.9 }      }

    }

    gene_nose_nostril_height = {

        1 = { name = nose_nostril_height   range = { 0.1 0.2 }      }
        7 = { name = nose_nostril_height   range = { 0.2 0.45 }      }
        84 = { name = nose_nostril_height   range = { 0.45 0.55 }      }
        7 = { name = nose_nostril_height   range = { 0.55 0.8 }      }
        1 = { name = nose_nostril_height   range = { 0.8 0.9 }      }

    }

    gene_nose_nostril_width = {


        7 = { name = nose_nostril_width   range = { 0.4 0.45 }      }
        54 = { name = nose_nostril_width   range = { 0.45 0.55 }      }
        57 = { name = nose_nostril_width   range = { 0.55 0.8 }      }      #slightly higher than normal, but not as high as dwarf or gnome ofc


    }

    gene_nose_ridge_angle = {

        1 = { name = nose_ridge_angle   range = { 0.1 0.2 }      }
        17 = { name = nose_ridge_angle   range = { 0.2 0.45 }      }
        64 = { name = nose_ridge_angle   range = { 0.45 0.55 }      }
        17 = { name = nose_ridge_angle   range = { 0.55 0.8 }      }
        1 = { name = nose_ridge_angle   range = { 0.8 0.9 }      }

    }

    gene_nose_ridge_def = {

        1 = { name = nose_ridge_def   range = { 0.1 0.2 }      }
        7 = { name = nose_ridge_def   range = { 0.2 0.45 }      }
        84 = { name = nose_ridge_def   range = { 0.45 0.55 }      }
        7 = { name = nose_ridge_def   range = { 0.55 0.8 }      }
        1 = { name = nose_ridge_def   range = { 0.8 0.9 }      }

    }

    gene_nose_ridge_def_min = {

        1 = { name = nose_ridge_def_min   range = { 0.1 0.2 }      }
        7 = { name = nose_ridge_def_min   range = { 0.2 0.45 }      }
        84 = { name = nose_ridge_def_min   range = { 0.45 0.55 }      }
        7 = { name = nose_ridge_def_min   range = { 0.55 0.8 }      }
        1 = { name = nose_ridge_def_min   range = { 0.8 0.9 }      }

    }

    gene_nose_ridge_width = {

        0 = { name = nose_ridge_width   range = { 0.1 0.2 }      }
        7 = { name = nose_ridge_width   range = { 0.2 0.45 }      }
        84 = { name = nose_ridge_width   range = { 0.45 0.55 }      }
        7 = { name = nose_ridge_width   range = { 0.55 0.8 }      }
        1 = { name = nose_ridge_width   range = { 0.8 0.9 }      }

    }

    gene_nose_size = {

        0 = { name = nose_size   range = { 0.1 0.2 }      }
        7 = { name = nose_size   range = { 0.2 0.45 }      }
        74 = { name = nose_size   range = { 0.45 0.55 }      }
        27 = { name = nose_size   range = { 0.55 0.8 }      }       #slightly higher chance of bigger nose
        1 = { name = nose_size   range = { 0.8 0.9 }      }

    }

    gene_nose_tip_angle = {

 
        84 = { name = nose_tip_angle   range = { 0.45 0.55 }      }     #more likely to tip up
        17 = { name = nose_tip_angle   range = { 0.55 0.8 }      }
        1 = { name = nose_tip_angle   range = { 0.8 0.9 }      }

    }

    gene_nose_tip_forward = {

        1 = { name = nose_tip_forward   range = { 0.1 0.2 }      }
        7 = { name = nose_tip_forward   range = { 0.2 0.45 }      }
        84 = { name = nose_tip_forward   range = { 0.45 0.55 }      }
        7 = { name = nose_tip_forward   range = { 0.55 0.8 }      }
        1 = { name = nose_tip_forward   range = { 0.8 0.9 }      }

    }

    gene_nose_tip_width = {

        54 = { name = nose_tip_width   range = { 0.5 0.6 }      }
        57 = { name = nose_tip_width   range = { 0.6 0.7 }      }  #always fat width
        1 = { name = nose_tip_width   range = { 0.7 0.9 }      }

    }

	gene_bs_body_type = {

        7 = { name = body_fat_head_fat_low   range = { 0.3 0.45 }      }    #less emaciated in general
        20 = { name = body_fat_head_fat_low   range = {0.45 0.6 }     }
        7 = { name = body_fat_head_fat_low   range = { 0.6 0.8 }     }

        7 = { name = body_fat_head_fat_medium   range = { 0.3 0.45 }      }
        20 = { name = body_fat_head_fat_medium   range = {0.45 0.6 }      }
        7 = { name = body_fat_head_fat_medium   range = { 0.6 0.8 }      }

        27 = { name = body_fat_head_fat_full   range = { 0.3 0.45 }      }   #higher chance of full fat
        40 = { name = body_fat_head_fat_full   range = {0.45 0.6 }      }
        27 = { name = body_fat_head_fat_full   range = { 0.6 0.8 }      }  #higher big lad threshold

	}

	gene_height = {

        20 = { name = short_height  range = { 0.1 0.2  }      }
        50 = { name = short_height  range = { 0.2 0.3 }      }
        20 = { name = short_height  range = { 0.3 0.5 }      }

	}

	gene_old_eyes = {

		10 = { name = old_eyes_01   range = { 1.0 1.0 }      }
		10 = { name = old_eyes_02   range = { 1.0 1.0 }      }
		10 = { name = old_eyes_03   range = { 1.0 1.0 }      }
	}

	gene_old_forehead = {

		10 = { name = old_forehead_01   range = { 1.0 1.0 }      }
		10 = { name = old_forehead_02   range = { 1.0 1.0 }      }
		10 = { name = old_forehead_03   range = { 1.0 1.0 }      }
	}

	gene_old_mouth = {

		10 = { name = old_mouth_01   range = { 1.0 1.0 }      }
		10 = { name = old_mouth_02   range = { 1.0 1.0 }      }
		10 = { name = old_mouth_03   range = { 1.0 1.0 }      }
	}

	gene_complexion = {

		10 = { name = complexion_01   range = { 0.0 0.0 }      }
		10 = { name = complexion_02   range = { 0.0 0.0 }      }
		10 = { name = complexion_03   range = { 0.0 0.0 }      }
	}

	gene_crowfeet = {

		9 = { name = crowfeet_01   range = { 0.0 0.1 }      }
		1 = { name = crowfeet_01   range = { 0.1 0.8 }      }
		9 = { name = crowfeet_02   range = { 0.0 0.1 }      }
		1 = { name = crowfeet_02   range = { 0.1 0.8 }      }
		9 = { name = crowfeet_03   range = { 0.0 0.1 }      }
		1 = { name = crowfeet_03   range = { 0.1 0.8 }      }
	}

	gene_frown = {

		8 = { name = frown_01   range = { 0.0 0.1 }      }
		2 = { name = frown_01   range = { 0.1 0.8 }      }
		8 = { name = frown_02   range = { 0.0 0.1 }      }
		2 = { name = frown_02   range = { 0.1 0.8 }      }
		8 = { name = frown_03   range = { 0.0 0.1 }      }
		2 = { name = frown_03   range = { 0.1 0.8 }      }
	}

	gene_surprise = {

		9 = { name = surprise_01   range = { 0.0 0.1 }      }
		1 = { name = surprise_01   range = { 0.1 0.8 }      }
		9 = { name = surprise_02   range = { 0.0 0.1 }      }
		1 = { name = surprise_02   range = { 0.1 0.8 }      }
		9 = { name = surprise_03   range = { 0.0 0.1 }      }
		1 = { name = surprise_03   range = { 0.1 0.8 }      }
	}

    gene_eyebrows_shape = {
        22 = {  name = avg_spacing_avg_thickness             range = { 0.5 1.0 }     }
        44 = {  name = avg_spacing_high_thickness             range = { 0.5 1.0 }     }
        10 = {  name = avg_spacing_low_thickness             range = { 0.5 1.0 }    }
        5 = {  name = avg_spacing_lower_thickness             range = { 0.5 1.0 }    }

        22 = {  name = far_spacing_avg_thickness             range = { 0.5 1.0 }     }
        44 = {  name = far_spacing_high_thickness             range = { 0.5 1.0 }     }      #bushy eyebrow folk
        10 = {  name = far_spacing_low_thickness             range = { 0.5 1.0 }    }
        5 = {  name = far_spacing_lower_thickness             range = { 0.5 1.0 }    }

        22 = {  name = close_spacing_avg_thickness             range = { 0.5 1.0 }     }
        44 = {  name = close_spacing_high_thickness             range = { 0.5 1.0 }     }
        10 = {  name = close_spacing_low_thickness             range = { 0.5 1.0 }    }
        5 = {  name = close_spacing_lower_thickness             range = { 0.5 1.0 }    }
    }

    gene_eyebrows_fullness = {
        10 = {  name = layer_2_avg_thickness             range = { 0.25 0.5 }     }
        15 = {  name = layer_2_avg_thickness             range = { 0.5 0.75 }     }
        25 = {  name = layer_2_high_thickness             range = { 0.25 0.5 }     }
        25 = {  name = layer_2_high_thickness             range = { 0.5 0.75 }     }
        10 = {  name = layer_2_low_thickness             range = { 0.25 0.5 }     }
        15 = {  name = layer_2_low_thickness             range = { 0.5 0.75 }     }
        10 = {  name = layer_2_lower_thickness             range = { 0.25 0.5 }     }
        15 = {  name = layer_2_lower_thickness             range = { 0.5 0.75 }     }
    }

    gene_face_dacals = {

        10 = { name = face_dacal_01   range = { 1.0 1.0 }      }
        10 = { name = face_dacal_02   range = { 1.0 1.0 }      }
        10 = { name = face_dacal_03   range = { 1.0 1.0 }      }
        10 = { name = face_dacal_04   range = { 1.0 1.0 }      }
        10 = { name = face_dacal_05   range = { 1.0 1.0 }      }
 
    }

	hairstyles = {
		10 = { name = halfling_hairstyles 		range = { 0.0 1.0 } }
	}

	beards = {
		10 = { name = halfling_beards                      range = { 0.0 1.0 } }
        10 = { name = no_beard                      range = { 0.0 1.0 } }
	}

	mustaches = {
        10 = { name = no_mustache       range = { 0.0 1.0 } }   #no mustaches!
	}

	# coats = {
	# 	10 = { name = prussian_coats		range = { 0.0 1.0 } }
	# }

	# epaulettes = {
	# 	10 = { name = prussian_epaulettes		range = { 0.0 1.0 } }
	# }

	# sashes = {
	# 	10 = { name = prussian_sashes		range = { 0.0 1.0 } }
	# }

	# medals = {
	# 	10 = { name = all_medals		range = { 0.0 1.0 } }
	# }

	# headgear = {
	# 	10 = { name = generic_headgear		range = { 0.0 1.0 } }
	# }


    #Anbennar Additions my friends

	POD_NOSschnoz = {
		10 = { name = nosies 		range = { 0.0 0.0 } }
        10 = { name = schnozies       range = { 0.0 0.0 } }
	}


}
