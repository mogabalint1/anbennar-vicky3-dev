BUILDINGS={
	#Magharma
	s:STATE_SKURKHA_KYARD = {
		region_state:E02 = {
			create_building={
				building="building_rye_farm"
				level=2
				reserves=1
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_logging_camp"
				level=4
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
			}
			create_building={
				building="building_barracks"
				level=10
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	s:STATE_OVTO_KIGVAL = {
		region_state:E02 = {
			create_building={
				building="building_rye_farm"
				level=2
				reserves=1
			}
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building="building_barracks"
				level=5
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	s:STATE_KVAKEINOLBA = {
		region_state:E02 = {
			create_building={
				building="building_rye_farm"
				level=2
				reserves=1
			}
			create_building={
				building="building_livestock_ranch"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				ctivate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
			}
			create_building={
				building="building_paper_mills"
				level=2
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_privately_owned_building_paper_mills" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_barracks"
				level=5
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	s:STATE_DZIMOKLI = {
		region_state:E02 = {
			create_building={
				building="building_rye_farm"
				level=3
				reserves=1
			}
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_coal_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_coal_mine" }
			}
			create_building={
				building="building_food_industry"
				level=2
				reserves=1
				activate_production_methods={ "pm_disabled_canning" "pm_bakery" "pm_manual_dough_processing" "pm_disabled_distillery" }
			}
			create_building={
				building="building_tooling_workshops"
				level=1
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" "pm_privately_owned_building_tooling_workshops" }
			}
			create_building={
				building="building_government_administration"
				level=2
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
			create_building={
				building="building_barracks"
				level=5
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	s:STATE_FOIRAKHIAN = {
		region_state:E02 = {
			create_building={
				building="building_rye_farm"
				level=1
				reserves=1
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_barracks"
				level=5
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	s:STATE_SHEVROMRZGH = {
		region_state:E02 = {
			create_building={
				building="building_rye_farm"
				level=1
				reserves=1
			}
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_cotton_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_textile_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_no_luxury_clothes" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_barracks"
				level=10
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	s:STATE_CAUBHEAMEAS = {
		region_state:E02 = {
			#blank
		}
		region_state:E06 = {
			#blank
		}
	}
	
	#Centaurs
	s:STATE_SERPENT_GIFT = {
		region_state:E05 = {
			create_building={
				building="building_wheat_farm" #only state in FP with wheat
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools" }
			}
			create_building={
				building="building_sugar_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_opium_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_opium_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_barracks"
				level=15
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	s:STATE_SARLUN_GOILUST = {
		region_state:E05 = {
			#blank
		}
		region_state:E03 = {
			#blank
		}
	}
	s:STATE_UGHEABAR = {
		region_state:E08 = {
			create_building={
				building="building_opium_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_opium_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_barracks"
				level=20
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	s:STATE_NAGLAIBAR = {
		region_state:E08 = {
			#blank
		}
		region_state:E03 = {
			#blank
		}
	}
	s:STATE_IRDU_AGEENEAS = {
		region_state:E07 = {
			#blank
		}
	}
	s:STATE_GHANEERSP = {
		region_state:E07 = {
			create_building={
				building="building_rye_farm"
				level=1
				reserves=1
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_lead_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_lead_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_lead_mine" }
			}
			create_building={
				building="building_barracks"
				level=10
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	
	#Orachav/Cossack
	s:STATE_SOCHULEAG = {
		region_state:E03 = {
			create_building={
				building="building_cotton_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_barracks"
				level=10
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}
	s:STATE_ADOI_FILEANAN = {
		region_state:E03 = {
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_barracks"
				level=10
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	s:STATE_ORCHEKH = {
		region_state:E04 = {
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_barracks"
				level=10
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	s:STATE_APORLAEN = {
		region_state:E04 = {
			create_building={
				building="building_barracks"
				level=10
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}
	#Federation Nuzurbokh
	s:STATE_ZARMIKLON = {
		region_state:E01 = {
			#blank
		}
	}
	s:STATE_BULREKAYIG = {
		region_state:E01 = {
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
		}
	}
	s:STATE_AKANDHIL = {
		region_state:E01 = {
			#blank
		}
	}
	s:STATE_TOZGONREK = {
		region_state:E01 = {
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
		}
	}
	s:STATE_ZOITAL = {
		region_state:E01 = {
			#blank
		}
	}
	s:STATE_ZUURZOI = {
		region_state:E01 = {
			#blank
		}
	}
	#Federation Island
	s:STATE_PEENADHI = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				level=2
				reserves=1
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_tea_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_dye_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
			}
			create_building={
				building="building_textile_mills"
				level=3
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_craftsman_sewing" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_shipyards"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding" "pm_merchant_guilds_building_shipyards" "pm_military_shipbuilding_wooden" }
			}
			create_building={
				building="building_government_administration"
				level=3
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_barracks"
				level=4
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
			create_building={
				building="building_naval_base"
				level=2
				reserves=1
				activate_production_methods={ "pm_man_o_wars" "pm_frigates" "pm_commerce_raider" "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}
	s:STATE_GADHLUMO = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				level=3
				reserves=1
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=3
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_tea_plantation"
				level=1
				reserves=2
				activate_production_methods={ "default_building_tea_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_dye_plantation"
				level=3
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building="building_fishing_wharf"
				level=2
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_tooling_workshops"
				level=1
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" "pm_privately_owned_building_tooling_workshops" }
			}
			create_building={
				building="building_government_administration"
				level=8
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_barracks"
				level=6
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
			create_building={
				building="building_naval_base"
				level=1
				reserves=1
				activate_production_methods={ "pm_man_o_wars" "pm_frigates" "pm_commerce_raider" "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=2
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}
	s:STATE_PRIKOYOL = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				level=3
				reserves=1
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=2
				reserves=1
			}
			create_building={
				building="building_tea_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_dye_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_fishing_wharf"
				level=3
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_textile_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_craftsman_sewing" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_paper_mills"
				level=3
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_merchant_guilds_building_paper_mills" }
			}
			create_building={
				building="building_government_administration"
				level=6
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_barracks"
				level=5
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
		}
	}
	s:STATE_YUKARON = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				level=4
				reserves=1
			}
			create_building={
				building="building_tobacco_plantation"
				level=2
				reserves=1
			}
			create_building={
				building="building_silk_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_silk_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_tea_plantation"
				level=1
				reserves=2
				activate_production_methods={ "default_building_tea_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_dye_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building="building_textile_mills"
				level=2
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_craftsman_sewing" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_paper_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_merchant_guilds_building_paper_mills" }
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			create_building={
				building="building_government_administration"
				level=4
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_barracks"
				level=4
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	s:STATE_YUKAROYOL = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				level=2
				reserves=1
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
			}
			create_building={
				building="building_tea_plantation"
				level=1
				reserves=2
				activate_production_methods={ "default_building_tea_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_logging_camp"
				level=9
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_textile_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_craftsman_sewing" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_glassworks"
				level=2
				reserves=1
				activate_production_methods={ "pm_forest_glass"  "pm_ceramics" "pm_manual_glassblowing" "pm_merchant_guilds_building_glassworks" }
			}
			create_building={
				building="building_paper_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_merchant_guilds_building_paper_mills" }
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			create_building={
				building="building_government_administration"
				level=3
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_barracks"
				level=2
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	s:STATE_QUSHYIL = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				level=3
				reserves=1
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=2
				reserves=1
			}
			create_building={
				building="building_tea_plantation"
				level=1
				reserves=2
				activate_production_methods={ "default_building_tea_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_textile_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_craftsman_sewing" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_shipyards"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding" "pm_merchant_guilds_building_shipyards" "pm_military_shipbuilding_wooden" }
			}
			create_building={
				building="building_government_administration"
				level=2
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_barracks"
				level=3
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
			create_building={
				building="building_naval_base"
				level=1
				reserves=1
				activate_production_methods={ "pm_man_o_wars" "pm_frigates" "pm_commerce_raider" "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}
	s:STATE_KESH_GOLKHIN = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				level=3
				reserves=1
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_silk_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_silk_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_tea_plantation"
				level=1
				reserves=3
				activate_production_methods={ "default_building_tea_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
			}
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_glassworks"
				level=4
				reserves=1
				activate_production_methods={ "pm_forest_glass"  "pm_ceramics" "pm_manual_glassblowing" "pm_merchant_guilds_building_glassworks" }
			}
			create_building={
				building="building_tooling_workshops"
				level=1
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" "pm_privately_owned_building_tooling_workshops" }
			}
			create_building={
				building="building_paper_mills"
				level=3
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_merchant_guilds_building_paper_mills" }
			}
			create_building={
				building="building_shipyards"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding" "pm_merchant_guilds_building_shipyards" "pm_military_shipbuilding_wooden" }
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			create_building={
				building="building_government_administration"
				level=12
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_barracks"
				level=5
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
			create_building={
				building="building_naval_base"
				level=1
				reserves=1
				activate_production_methods={ "pm_man_o_wars" "pm_frigates" "pm_commerce_raider" "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
		}
	}
	s:STATE_SARTZ_NEISAR = {
		region_state:E01 = {
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_tea_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_whaling_station"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_whaling_ships" "pm_unrefrigerated" "pm_merchant_guilds_building_whaling_station" }
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			create_building={
				building="building_government_administration"
				level=3
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_barracks"
				level=3
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}
	s:STATE_SHIKENKHIIN = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				level=3
				reserves=1
			}
			create_building={
				building="building_silk_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_silk_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_tea_plantation"
				level=1
				reserves=2
				activate_production_methods={ "default_building_tea_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_gold_mine"
				level=4
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_gold_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_gold_mine" }
			}
			create_building={
				building="building_glassworks"
				level=2
				reserves=1
				activate_production_methods={ "pm_forest_glass"  "pm_ceramics" "pm_manual_glassblowing" "pm_merchant_guilds_building_glassworks" }
			}
			create_building={
				building="building_paper_mills"
				level=3
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_merchant_guilds_building_paper_mills" }
			}
			create_building={
				building="building_government_administration"
				level=9
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_barracks"
				level=3
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	s:STATE_ZOI_KHORKHIIN = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				level=1
				reserves=1
			}
			create_building={
				building="building_livestock_ranch"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_tea_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_logging_camp"
				level=8
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building="building_whaling_station"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_whaling_ships" "pm_unrefrigerated" "pm_merchant_guilds_building_whaling_station" }
			}
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_paper_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_merchant_guilds_building_paper_mills" }
			}
			create_building={
				building="building_government_administration"
				level=2
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_barracks"
				level=2
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
		}
	}
	s:STATE_SHIK_DAZAR = {
		region_state:E01 = {
			create_building={
				building="building_rye_farm"
				level=3
				reserves=1
			}
			create_building={
				building="building_silk_plantation"
				level=3
				reserves=1
				activate_production_methods={ "default_building_silk_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_tea_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tea_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			create_building={
				building="building_whaling_station"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_whaling_ships" "pm_unrefrigerated" "pm_merchant_guilds_building_whaling_station" }
			}
			create_building={
				building="building_fishing_wharf"
				level=2
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_textile_mills"
				level=2
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_craftsman_sewing" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_furniture_manufacturies"
				level=3
				reserves=1
				activate_production_methods={ "pm_lathe" "pm_privately_owned_building_furniture_manufacturies" "pm_automation_disabled" "pm_no_luxuries" }
			}
			create_building={
				building="building_glassworks"
				level=1
				reserves=1
				activate_production_methods={ "pm_forest_glass"  "pm_ceramics" "pm_manual_glassblowing" "pm_merchant_guilds_building_glassworks" }
			}
			create_building={
				building="building_paper_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_merchant_guilds_building_paper_mills" }
			}
			create_building={
				building="building_shipyards"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding" "pm_merchant_guilds_building_shipyards" "pm_military_shipbuilding_wooden" }
			}
			create_building={
				building="building_university"
				level=2
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			create_building={
				building="building_government_administration"
				level=4
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_barracks"
				level=3
				reserves=1
				activate_production_methods={ "pm_irregular_infantry" "pm_no_artillery" "pm_cavalry_scouts" "pm_no_specialists" "pm_wound_dressing" }
			}
			create_building={
				building="building_naval_base"
				level=1
				reserves=1
				activate_production_methods={ "pm_man_o_wars" "pm_frigates" "pm_commerce_raider" "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
		}
	}
}