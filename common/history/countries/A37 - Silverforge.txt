﻿COUNTRIES = {
	c:A37 = {
		effect_starting_technology_tier_2_tech = yes
		
		#From Anbennar laws
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_census_voting
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_elected_bureaucrats
		activate_law = law_type:law_professional_army

		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_right_of_assembly
		#activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_non_monstrous_only
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_traditional_magic_encouraged
		
		ig:ig_devout = {
			set_interest_group_name = ig_ravelian_church
			remove_ideology = ideology_moralist
			add_ideology = ideology_ravelian_moralist
			remove_ideology = ideology_pious
			add_ideology = ideology_scholarly
		}
	}
}