﻿COUNTRIES = {
	c:R13 = {
		effect_starting_technology_tier_2_tech = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_national_militia
		
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_dedicated_police #Militarized police not possible without mass surveillance
		activate_law = law_type:law_religious_schools
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_women_own_property #Women in workplace not possible without feminism
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_all_races_allowed
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_artifice_encouraged
	}
}