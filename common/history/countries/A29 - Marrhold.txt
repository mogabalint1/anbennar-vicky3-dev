﻿COUNTRIES = {
	c:A29 = {
		effect_starting_technology_tier_4_tech = yes	#doc says lagging behind rest of Escann in terms of institutions and economy
		add_technology_researched = line_infantry		#behind but adding some bits as they are still militarized
		add_technology_researched = army_reserves

		activate_law = law_type:law_monarchy	#absolutist monarchy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_traditionalism # Not allowed interventionism with serfdom
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		
		# No law enforcement means no censorship
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_traditional_magic_encouraged
	
		ig:ig_devout = {
			set_interest_group_name = ig_corinite_faithful
			remove_ideology = ideology_moralist
			add_ideology = ideology_corinite_moralist
			remove_ideology = ideology_patriarchal
			add_ideology = ideology_feminist_ig
			add_ideology = ideology_anti_slavery
		}
	}
}