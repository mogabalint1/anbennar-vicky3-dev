﻿COUNTRIES = {
	c:C03 = {
		effect_starting_technology_tier_4_tech = yes
		
		# Laws 
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_census_voting
		activate_law = law_type:law_national_supremacy # needed so that e.g. Poles are discriminated against
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_elected_bureaucrats
		activate_law = law_type:law_national_militia
		activate_law = law_type:law_national_guard
		
		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_dedicated_police
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_homesteading
		activate_law = law_type:law_women_own_property # Not allowed women in workplace without voting
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_same_race_only
		activate_law = law_type:law_traditional_magic_encouraged
	}
}