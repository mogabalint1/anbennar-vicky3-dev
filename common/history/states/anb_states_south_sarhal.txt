STATES = {

	s:STATE_DHAL_NIKHUVAD = {
		create_state = {
			country = c:L05
			owned_provinces = { "x637DB2" "xF94F81" "x92E66F" "x0E2C7E" "xD22CA7" }
		}
		
		add_homeland = 
	}

	s:STATE_DHEBIJ_JANAB = {
		create_state = {
			country = c:L05
			owned_provinces = { "x56F9EC" "x842FCB" "x371F8E" "x87F71A" }
		}
		
		add_homeland = 
	}

	s:STATE_SUHRATBA_YAJ = {
		create_state = {
			country = c:L05
			owned_provinces = { "x159F03" "x0962A6" "x7B0511" "x39FA16" "x6E50DC" }
		}
		
		add_homeland = 
	}

	s:STATE_QASRIYINGI = {
		create_state = {
			country = c:L05
			owned_provinces = { "x64EEAC" "x046160" "x54A798" "xE0C160" "x809D71" "xBF990C" }
		}
		
		add_homeland = 
	}
	
	s:STATE_DHEBIJ_DHEKA = {
		create_state = {
			country = c:L05
			owned_provinces = {  }
		}
		
		add_homeland = 
	}

	s:STATE_BAASHI_BADDA = {
		create_state = {
			country = c:L05
			owned_provinces = { "x25E624" "xC6F466" "x14F888" "x394174" "x53A1EB" }
		}
		
		add_homeland = 
	}
	
	s:STATE_SUHRATBA_YAHIN = {
		create_state = {
			country = c:L05
			owned_provinces = { "xF88745" "x23DAFA" "x22FB02" "x512A4E" }
		}
		
		add_homeland = 
	}

	s:STATE_DHAL_TANIZUUD = {
		create_state = {
			country = c:L06
			owned_provinces = { "xDAB2E8" "x7B5A09" "x7F137D" "x0BFAB5" }
		}
		
		add_homeland = 
	}

	s:STATE_JURITAQA = {
		create_state = {
			country = c:L06
			owned_provinces = { "x7A8026" "x32E2D3" "xB69B54" }
		}
		
		add_homeland = 
	}

	s:STATE_MPAKA = {
		create_state = {
			country = c:L06
			owned_provinces = { "xF2EFEA" "x6CEA94" "x3D48BC" }
		}
		
		add_homeland = 
	}

	s:STATE_DEBIJ_SHAR = {
		create_state = {
			country = c:L05
			owned_provinces = { "x736D8D" "x35042C" "x0CBC46" "xC977E5" }
		}
		create_state = {
			country = c:L07
			owned_provinces = { "x61C75E" }
		}
		create_state = {
			country = c:L08
			owned_provinces = { "x2F700F" }
		}
		
		add_homeland = 
	}

	s:STATE_ASHAMAD_BARIGA = {
		create_state = {
			country = c:L08
			owned_provinces = { "x0C7167" "xBA14D0" "xF14D7F" "xD98049" "x888DF8" }
		}
		
		add_homeland = 
	}
	
	s:STATE_DHAI_BAEIDAG = {
		create_state = {
			country = c:L08
			owned_provinces = { "xE8C9F9" "x8F515C" "xB19085" }
		}
		
		add_homeland = 
	}

	s:STATE_QASRI_ABEESOOYINKA = {
		create_state = {
			country = c:L07
			owned_provinces = { "x896517" "x12C30C" "x23B901" "x3D0E08" "x6477B7" "xEBCB1F" "xE936A7" "x48CB0C" }
		}
		
		add_homeland = 
	}
	
	s:STATE_QAYNSLAND = {
		create_state = {
			country = c:L09
			owned_provinces = { "x67B72F" "x4DC795" "xCDB694" "x02A0ED" "xE3773D" "x89017B" "xBDF7C5" "x6B35A3" "x9D99B5" }
		}
		
		add_homeland = 
	}

	s:STATE_THE_OHITS = {
		create_state = {
			country = c:L09
			owned_provinces = { "xB34FB5" "xE4403D" "xD6BD94" "x2D93E0" "xF75DB2" "x5B1BD8" "xA703FF" "xE27C89" }
		}
		
		add_homeland = 
	}

	s:STATE_HARENMARCHES = {
		create_state = {
			country = c:L10
			owned_provinces = { "xCDE994" "xDC74C9" "x3DE367" "x560617" "x8FE18C" "x718F0D" }
		}
		
		add_homeland = 
	}

	s:STATE_SAMAANIA = {
		create_state = {
			country = c:L11
			owned_provinces = { "x6B834D" "x680D38" "x0BDF26" "x2187DF" }
		}
		
		add_homeland = 
	}

	s:STATE_ELIANDE = {
		create_state = {
			country = c:L12
			owned_provinces = { "xB06E77" "x996B08" "x739AD2" }
		}
		create_state = {
			country = c:L18
			owned_provinces = { "x635EA2" } 
		}
		
		add_homeland = 
	}

	s:STATE_HADEADOL = {
		create_state = {
			country = c:L12
			owned_provinces = { "xC6667E" "x46C83A" "xF7EAB1" }
		}
		create_state = {
			country = c:L13
			owned_provinces = { "x9CC8D6" "x6FC76B" "xCB8D15" "x08606B" }
		}
		create_state = {
			country = c:L14
			owned_provinces = { "xAA4D42" "x1C09ED" }
		}
		
		add_homeland = 
	}

	s:STATE_HARASCILDE = {
		create_state = {
			country = c:L15
			owned_provinces = { "xC88E52" "xC1BD3D" "xD1FD51" "x8A5057" "x5F6263" }
		}
		create_state = {
			country = c:L16
			owned_provinces = { "xEC0A7C" "x7406B0" }
		}
		
		add_homeland = 
	}

	s:STATE_OLD_JINNAKAH = {
		create_state = {
			country = c:L05
			owned_provinces = { "xB3D418" "xC9E1DE" "x397C06" }
		}
		create_state = {
			country = c:L16
			owned_provinces = { "xC25143" }
		}
		create_state = {
			country = c:L19
			owned_provinces = { "x49247E" }
		}
		
		add_homeland = 
	}

	s:STATE_SSIPPANSEK = {
		create_state = {
			country = c:L07
			owned_provinces = { "xC974B1" "xDFBCEA" "x102158" "x9EAD2A" }
		}
		
		add_homeland = 
	}
}