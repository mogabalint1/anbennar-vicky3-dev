CHARACTERS = {
	c:B22 = {
		create_character = {
			first_name = "Nuriye"
			last_name = szel_Aqatzan
			ruler = yes
			noble = yes
			female = yes
			age = 15
			interest_group = ig_petty_bourgeoisie
			ideology = ideology_moderate
			traits = {
				reserved psychological_affliction
			}
		}
	}
}
