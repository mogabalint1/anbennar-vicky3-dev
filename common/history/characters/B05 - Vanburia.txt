﻿CHARACTERS = {
	c:B05 = {
		create_character = {
			#role = politician # Remove this line
			first_name = "Dalyon"
			last_name = "of_Vanbury"
			ruler = yes
			age = 42
			interest_group = ig_industrialists
			ideology = ideology_royalist
			traits = {
				ambitious innovative
			}
		}
	}
}
