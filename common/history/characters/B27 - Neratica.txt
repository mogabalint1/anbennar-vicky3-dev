CHARACTERS = {
	c:B27 = {
		create_character = {
			first_name = "Emil"
			last_name = sil_Onyx
			ruler = yes
			age = 38
			interest_group = ig_devout
			ideology = ideology_moderate
			traits = {
				wrathful
			}
		}
	}
}
