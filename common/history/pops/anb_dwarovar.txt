﻿POPS = {
	s:STATE_HJYRTSIKKI = {
		region_state:A10 = {
			create_pop = {
				culture = grombari_half_orc
				size = 68579
			}
			create_pop = {
				culture = grombari_orc
				size = 62393
				split_religion = {
					grombari_orc = {
						corinite = 0.5
						old_dookan = 0.5
					}
				}
			}
			create_pop = {
				culture = forest_troll
				size = 12611
			}
		}
	}


	s:STATE_FRIGID_FOREST = {
		region_state:A10 = {
			create_pop = {
				culture = grombari_orc
				size = 313138
				split_religion = {
					grombari_orc = {
						corinite = 0.8
						old_dookan = 0.2
					}
				}
			}
			create_pop = {
				culture = grombari_half_orc
				size = 331006
			}
			create_pop = {
				culture = forest_troll
				size = 37491
			}
		}
	}

	s:STATE_COPPERWOOD = {
		region_state:A10 = {
			create_pop = {
				culture = grombari_half_orc
				size = 354695
			}
			create_pop = {
				culture = grombari_orc
				size = 396702
				split_religion = {
					grombari_orc = {
						corinite = 0.7
						old_dookan = 0.3
					}
				}
			}
			create_pop = {
				culture = mossmouth_ogre
				size = 34344
				split_religion = {
					mossmouth_ogre = {
						corinite = 0.8
						feast_of_the_gods = 0.2
					}
				}
			}
			create_pop = {
				culture = black_orc
				size = 37864
				split_religion = {
					black_orc = {
						corinite = 0.5
						old_dookan = 0.5
					}
				}
			}
		}
	}

	s:STATE_KHUGSVALE = {
		region_state:A10 = {
			create_pop = {
				culture = grombari_half_orc
				size = 238511
			}
			create_pop = {
				culture = grombari_orc
				size = 229396
			}
			create_pop = {
				culture = black_orc
				size = 42046
				split_religion = {
					black_orc = {
						corinite = 0.2
						old_dookan = 0.8
					}
				}
			}
		}
	}
	s:STATE_LONELY_MOUNTAIN = {
		region_state:A10 = {
			create_pop = {
				culture = cave_goblin
				size = 89445
			}
			create_pop = {
				culture = cave_troll
				size = 12731
			}
		}
	}
	s:STATE_TANNING_VALLEY = {
		region_state:A10 = {
			create_pop = {
				culture = mossmouth_ogre
				size = 26413
				split_religion = {
					mossmouth_ogre = {
						corinite = 0.9
						feast_of_the_gods = 0.1
					}
				}
			}
		}
	}
}