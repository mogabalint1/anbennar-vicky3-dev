﻿POPS = {
	s:STATE_EKYU_1 = {
		region_state:B43 = {
			create_pop = {
				culture = chiunife
				size = 100
			}
			create_pop = {
				culture = harafe
				size = 100
			}
		}
		region_state:B10 = {
			create_pop = {
				culture = harafe
				size = 100
			}
			create_pop = {
				culture = chiunife
				size = 100
			}
		}
	}
	s:STATE_EKYU_2 = {
		region_state:B43 = {
			create_pop = {
				culture = chiunife
				size = 100
			}
		}
	}
	s:STATE_EKYU_3 = {
		region_state:B43 = {
			create_pop = {
				culture = chiunife
				size = 100
			}
		}
		region_state:B40 = {
			create_pop = {
				culture = chiunife
				size = 100
			}
			create_pop = {
				culture = epednar
				size = 100
			}
		}
	}
	s:STATE_EKYU_4 = {
		region_state:B43 = {
			create_pop = {
				culture = chiunife
				size = 100
			}
		}
		region_state:B40 = {
			create_pop = {
				culture = chiunife
				size = 100
			}
			create_pop = {
				culture = epednar
				size = 100
			}
		}
	}
}