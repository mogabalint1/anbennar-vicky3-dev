﻿POPS = {
	s:STATE_STEEL_BAY = {
		region_state:B05 = {
			create_pop = {
				culture = vanburian
				size = 100
			}
		}
	}
	s:STATE_GREENHILL = {
		region_state:B05 = {
			create_pop = {
				culture = vanburian
				size = 100
			}
		}
	}
	s:STATE_RYAIL = {
		region_state:B05 = {
			create_pop = {
				culture = vanburian
				size = 100
			}
		}
	}
	s:STATE_SCATTERED_ISLANDS = {
		region_state:B05 = {
			create_pop = {
				culture = vanburian
				size = 100
			}
		}
	}
	s:STATE_ARPOLAIL = {
		region_state:B03 = {
			create_pop = {
				culture = vanburian
				size = 100
			}
		}
	}
	s:STATE_BRISTAILEAN = {
		region_state:B03 = {
			create_pop = {
				culture = arbarani
				size = 100
			}
		}
		region_state:A08 = {
			create_pop = {
				culture = tefori
				size = 100
			}
		}
		region_state:B05 = {
			create_pop = {
				culture = vanburian
				size = 100
			}
		}
	}
	s:STATE_GUARDIAN_ISLANDS = {
		region_state:B08 = {
			create_pop = {
				culture = busilari
				size = 100
			}
			create_pop = {
				culture = derannic
				size = 100
			}
		}
		region_state:B07 = {
			create_pop = {
				culture = tinker_gnome
				size = 100
			}
		}
	}
	s:STATE_BENTER = {
		region_state:B09 = {
			create_pop = {
				culture = lorentish
				size = 100
			}
			create_pop = {
				culture = mayte
				size = 100
			}
		}
	}
	s:STATE_XAYBATENCOS = {
		region_state:B09 = {
			create_pop = {
				culture = vernman
				size = 100
			}
			create_pop = {
				culture = lorentish
				size = 100
			}
			create_pop = {
				culture = mayte
				size = 100
			}
		}
	}
	s:STATE_TERRACEWOODS = {
		region_state:B09 = {
			create_pop = {
				culture = lorentish
				size = 100
			}
			create_pop = {
				culture = mayte
				size = 100
			}
		}
		region_state:B66 = {
			create_pop = {
				culture = mayte
				size = 100
			}
		}
		region_state:B03 = {
			create_pop = {
				culture = vernman
				size = 100
			}
			create_pop = {
				culture = mayte
				size = 100
			}
		}
	}
	s:STATE_MAYTE = {
		region_state:B03 = {
			create_pop = {
				culture = arbarani
				size = 100
			}
			create_pop = {
				culture = vernman
				size = 100
			}
			create_pop = {
				culture = mayte
				size = 100
			}
		}
	}
	s:STATE_TLACHIBAR = {
		region_state:B66 = {
			create_pop = {
				culture = mayte
				size = 100
			}
		}
		region_state:B70 = {
			create_pop = {
				culture = mayte
				size = 100
			}
		}
		region_state:B11 = {
			create_pop = {
				culture = epednar
				size = 100
			}
			create_pop = {
				culture = mayte
				size = 100
			}
		}
	}
	s:STATE_TAMETER = {
		region_state:B11 = {
			create_pop = {
				culture = epednar
				size = 100
			}
			create_pop = {
				culture = mayte
				size = 100
			}
		}
	}
	s:STATE_WILIKINAH = {
		region_state:B11 = {
			create_pop = {
				culture = harafe
				size = 100
			}
			create_pop = {
				culture = epednar
				size = 100
			}
		}
		region_state:B10 = {
			create_pop = {
				culture = harafe
				size = 100
			}
		}
		region_state:B67 = {
			create_pop = {
				culture = mayte
				size = 100
			}
			create_pop = {
				culture = harafe
				size = 100
			}
		}
	}
	s:STATE_NEANKINAH = {
		region_state:B67 = {
			create_pop = {
				culture = mayte
				size = 100
			}
			create_pop = {
				culture = harafe
				size = 100
			}
		}
		region_state:B10 = {
			create_pop = {
				culture = harafe
				size = 100
			}
		}
		region_state:B73 = {
			create_pop = {
				culture = mayte
				size = 100
			}
			create_pop = {
				culture = harafe
				size = 100
			}
		}
	}
	s:STATE_LEETIKINAH = {
		region_state:B10 = {
			create_pop = {
				culture = harafe
				size = 100
			}
		}
	}
	s:STATE_MANGROVY_COAST = {
		region_state:B71 = {
			create_pop = {
				culture = pearlsedger
				size = 100
			}
			create_pop = {
				culture = mayte
				size = 100
			}
		}
		region_state:B73 = {
			create_pop = {
				culture = mayte
				size = 100
			}
		}
		region_state:B03 = {
			create_pop = {
				culture = mayte
				size = 100
			}
		}
	}
	s:STATE_NAUFKINAH = {
		region_state:B72 = {
			create_pop = {
				culture = harafe
				size = 100
			}
		}
		region_state:B73 = {
			create_pop = {
				culture = mayte
				size = 100
			}
			create_pop = {
				culture = harafe
				size = 100
			}
		}
		region_state:B69 = {
			create_pop = {
				culture = kooras
				size = 100
			}
		}
		region_state:B71 = {
			create_pop = {
				culture = pearlsedger
				size = 100
			}
			create_pop = {
				culture = kooras
				size = 100
			}
		}
		region_state:B10 = {
			create_pop = {
				culture = harafe
				size = 100
			}
		}
	}
	s:STATE_YEAKOLEO = {
		region_state:B07 = {
			create_pop = {
				culture = steelscale_kobold
				size = 100
			}
			create_pop = {
				culture = kooras
				size = 100
			}
		}
	}
	s:STATE_ZURZUMEXIA = {
		region_state:B07 = {
			create_pop = {
				culture = steelscale_kobold
				size = 100
			}
			create_pop = {
				culture = kooras
				size = 100
			}
		}
	}
	s:STATE_SPORK_RALD = {
		region_state:B07 = {
			create_pop = {
				culture = steelscale_kobold
				size = 100
			}
			create_pop = {
				culture = soot_goblin
				size = 100
			}
			create_pop = {
				culture = kooras
				size = 100
			}
		}
	}
	s:STATE_MESTIKARDU = {
		region_state:B07 = {
			create_pop = {
				culture = soot_goblin
				size = 100
			}
			create_pop = {
				culture = kooras
				size = 100
			}
		}
	}
	s:STATE_RAZPAK = {
		region_state:B07 = {
			create_pop = {
				culture = soot_goblin
				size = 100
			}
			create_pop = {
				culture = kooras
				size = 100
			}
		}
	}
	s:STATE_MIKIDAP = {
		region_state:B07 = {
			create_pop = {
				culture = steelscale_kobold
				size = 100
			}
			create_pop = {
				culture = kooras
				size = 100
			}
		}
		region_state:B69 = {
			create_pop = {
				culture = kooras
				size = 100
			}
		}
	}
	s:STATE_TSANKINAH = {
		region_state:B74 = {
			create_pop = {
				culture = harafe
				size = 100
			}
		}
		region_state:B69 = {
			create_pop = {
				culture = kooras
				size = 100
			}
			create_pop = {
				culture = harafe
				size = 100
			}
		}
	}
	s:STATE_KABAHNKINAH = {
		region_state:B76 = {
			create_pop = {
				culture = vernman
				size = 100
			}
			create_pop = {
				culture = beefoot_halfling
				size = 100
			}
			create_pop = {
				culture = derannic
				size = 100
			}
			create_pop = {
				culture = caamas
				size = 100
			}
			create_pop = {
				culture = tinker_gnome
				size = 100
			}
			create_pop = {
				culture = harafe
				size = 100
			}
		}
		region_state:B75 = {
			create_pop = {
				culture = harafe
				size = 100
			}
		}
		region_state:B74 = {
			create_pop = {
				culture = harafe
				size = 100
			}
		}
	}
	s:STATE_DRABS_CAMBANN = {
		region_state:B07 = {
			create_pop = {
				culture = tinker_gnome
				size = 100
			}
			create_pop = {
				culture = kooras
				size = 100
			}
		}
		region_state:B78 = {
			create_pop = {
				culture = kooras
				size = 100
			}
		}
		region_state:B76 = {
			create_pop = {
				culture = vernman
				size = 100
			}
			create_pop = {
				culture = beefoot_halfling
				size = 100
			}
			create_pop = {
				culture = derannic
				size = 100
			}
			create_pop = {
				culture = tinker_gnome
				size = 100
			}
			create_pop = {
				culture = kooras
				size = 100
			}
		}
		region_state:B75 = {
			create_pop = {
				culture = harafe
				size = 100
			}
			create_pop = {
				culture = kooras
				size = 100
			}
		}
	}
	s:STATE_KOORAS = {
		region_state:B07 = {
			create_pop = {
				culture = tinker_gnome
				size = 100
			}
			create_pop = {
				culture = soot_goblin
				size = 100
			}
			create_pop = {
				culture = kooras
				size = 100
			}
		}
		region_state:B78 = {
			create_pop = {
				culture = kooras
				size = 100
			}
		}
		region_state:B77 = {
			create_pop = {
				culture = kooras
				size = 100
			}
		}
		region_state:B75 = {
			create_pop = {
				culture = harafe
				size = 100
			}
			create_pop = {
				culture = kooras
				size = 100
			}
		}
	}
	s:STATE_GOMMIOCHAND = {
		region_state:B07 = {
			create_pop = {
				culture = tinker_gnome
				size = 100
			}
			create_pop = {
				culture = kooras
				size = 100
			}
		}
	}
	s:STATE_TALDOUD_KAYSA = {
		region_state:B07 = {
			create_pop = {
				culture = soot_goblin
				size = 100
			}
			create_pop = {
				culture = tinker_gnome
				size = 100
			}
			create_pop = {
				culture = kooras
				size = 100
			}
		}
		region_state:B77 = {
			create_pop = {
				culture = kooras
				size = 100
			}
		}
	}
	s:STATE_DESERT_OF_THE_HARAFE = {
		region_state:B05 = {
			create_pop = {
				culture = vanburian
				size = 100
			}
		}
		region_state:B79 = {
			create_pop = {
				culture = harafe
				size = 100
			}
			create_pop = {
				culture = chiunife
				size = 100
			}
		}
	}
}