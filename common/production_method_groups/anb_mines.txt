﻿pmg_mining_equipment_building_damestear_mine = {
	production_methods = {
		pm_picks_and_shovels_building_damestear_mine
		pm_atmospheric_engine_pump_building_damestear_mine
		pm_condensing_engine_pump_building_damestear_mine
		pm_diesel_pump_building_damestear_mine
	}
}

pmg_explosives_building_damestear_mine = {
	production_methods = {
		pm_no_explosives
		pm_nitroglycerin_building_damestear_mine
		pm_dynamite_building_damestear_mine
	}
}

pmg_steam_automation_building_damestear_mine = {
	production_methods = {
		pm_no_steam_automation
		pm_steam_donkey_mine
	}
}

pmg_train_automation_building_damestear_mine = {
	production_methods = {
		pm_road_carts
		pm_rail_transport_mine
	}
}

pmg_ownership_capital_building_damestear_mine = {
	production_methods = {
		pm_merchant_guilds_building_damestear_mine
		pm_privately_owned_building_damestear_mine
		pm_publicly_traded_building_damestear_mine
		pm_government_run_building_damestear_mine
		pm_worker_cooperative_building_damestear_mine
	}
}

pmg_base_building_damestear_fields = {
	production_methods = {
		default_building_damestear_fields
	}
}
