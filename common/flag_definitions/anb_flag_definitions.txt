﻿# FLAG_DEFINITION_LIST = {		# countries search for a list with the same name as their tag, the DEFAULT list is always included, if no flag definition is applicable for a country then its tag is used a COA_KEY
# 	includes = ANOTHER_LIST		# includes another list in this list, can be repeated
# 
# 	flag_definition = {			# the flag definitions that make up this list, can be repeated
# 		coa = [list] COA_KEY	# main flag, optional list keyword denotes a coa template
# 		allow_overlord_canton = yes				# default no
# 		coa_with_overlord_canton = <[list] coa>	# flag where a canton can be placed, optional list keyword same as above, defaults to coa
# 		overlord_canton_offset = { x y }		# canton placement offset, default { 0 0 }
# 		overlord_canton_scale = { x y }			# canton placement scale, default { 0.5 0.5 }
# 		subject_canton = [list] COA_KEY	# canton applied to subjects by this country, optional list keyword same as above
# 
# 		priority = value		# valid flag definition with the highest priority applies
# 		trigger = {}			# a trigger that determines if this flag definition is valid, see below for scope
#       allow_revolutionary_indicator = no      # Default = yes. If yes, a temporary revolutionary indicator will appear while the country is revolutionary
#       revolutionary_canton = [list] COA_KEY   # Optional. Default = default_revolutionary_canton. Defines which flag should be used as canton while this country is revolutionary
# 	}
# }

#            | existing country | releasing a country | country formation |
# |==========|==================|=====================|===================|
# |root      | definition       | definition          | definition        |
# |----------|------------------|---------------------|-------------------|
# |target    | country          | N/A                 | N/A               |
# |----------|------------------|---------------------|-------------------|
# |initiator | N/A              | player              | player            |
# |----------|------------------|---------------------|-------------------|
# |actor     | country          | player              | player            |
# |----------|------------------|---------------------|-------------------|
# |          | country's        |                     | player's          |
# |overlord  | direct overlord  | player              | direct overlord   |
# |          | if it exists     |                     | if it exists      |
# |----------|------------------|---------------------|-------------------|

# common variables
@coa_width = 768
@coa_height = 512
@canton_scale_cross_x = @[ ( 333 / coa_width ) + 0.001 ]
@canton_scale_cross_y = @[ ( 205 / coa_height ) + 0.001 ]
@canton_scale_sweden_x = @[ ( 255 / coa_width ) + 0.001 ]
@canton_scale_sweden_y = @[ ( 204 / coa_height ) + 0.001 ]
@canton_scale_norway_x = @[ ( 192 / coa_width ) + 0.001 ]
@canton_scale_norway_y = @[ ( 192 / coa_height ) + 0.001 ]
@canton_scale_denmark_x = @[ ( 220 / coa_width ) + 0.001 ]
@canton_scale_denmark_y = @[ ( 220 / coa_height ) + 0.001 ]
@third = @[1/3]


A01 = { # Anbennar
	flag_definition = {
		coa = A01
		subject_canton = A01
		allow_overlord_canton = yes
		coa_with_overlord_canton = A01_subject
		priority = 1
	}
	flag_definition = {
		coa = A01_absolute_monarchy
		subject_canton = A01_absolute_monarchy
		allow_overlord_canton = yes
		coa_with_overlord_canton = A01_subject
		priority = 20
		trigger = {
			coa_def_absolute_monarchy_flag_trigger = yes
		}
	}
	flag_definition = {
		coa = A01_republic
		subject_canton = A01_republic
		coa_with_overlord_canton = A01_subject
		priority = 10
		trigger = {
			coa_def_republic_flag_trigger = yes
		}
	}
	flag_definition = {
		coa = A01_communist
		subject_canton = A01_communist
		priority = 1500
		trigger = {
			coa_def_communist_flag_trigger = yes
		}
	}
}

A02 = { # Vivin Empire
	flag_definition = {
		coa = A02
		subject_canton = A02
		priority = 1
	}
	flag_definition = {
		coa = A02_absolute_monarchy
		subject_canton = A02_absolute_monarchy
		priority = 20
		trigger = {
			coa_def_absolute_monarchy_flag_trigger = yes
		}
	}
	flag_definition = {
		coa = A02_republic
		subject_canton = A02_republic
		priority = 10
		trigger = {
			coa_def_republic_flag_trigger = yes
		}
	}
	flag_definition = {
		coa = A02_communist
		subject_canton = A02_communist
		priority = 1500
		trigger = {
			coa_def_communist_flag_trigger = yes
		}
	}
}

A03 = { # Lorent
	flag_definition = {
		coa = A03
		subject_canton = A03
		priority = 1
	}
	#flag_definition = {
	#	coa = A03_absolute_monarchy
	#	subject_canton = A03_absolute_monarchy
	#	priority = 20
	#	trigger = {
	#		coa_def_absolute_monarchy_flag_trigger = yes
	#	}
	#}
	flag_definition = {
		coa = A03_republic
		subject_canton = A03_republic
		priority = 10
		trigger = {
			coa_def_republic_flag_trigger = yes
		}
	}
	flag_definition = {
		coa = A03_communist
		subject_canton = A03_communist
		priority = 1500
		trigger = {
			coa_def_communist_flag_trigger = yes
		}
	}
	flag_definition = {
		coa = A03_anarchy
		subject_canton = A03_anarchy
		priority = 1000
		trigger = {
			coa_def_anarchy_flag_trigger = yes
		}
	}
	flag_definition = {
		coa = A03_fascism
		subject_canton = A03_fascism
		priority = 1500
		trigger = {
			coa_def_fascist_flag_trigger = yes
		}
	}
}

A04 = { # Northern League
	flag_definition = {
		coa = A04
		subject_canton = A04
		priority = 1
	}
	flag_definition = {
		coa = A04_dalr
		subject_canton = A04_dalr
		priority = 2
		trigger = {
			scope:target = {
				country_has_primary_culture = cu:dalr
			}
		}
	}
	#flag_definition = {
	#	coa = A04_absolute_monarchy
	#	subject_canton = A04_absolute_monarchy
	#	priority = 20
	#	trigger = {
	#		coa_def_absolute_monarchy_flag_trigger = yes
	#	}
	#}
	flag_definition = {
		coa = A04_republic
		subject_canton = A04_republic
		priority = 10
		trigger = {
			coa_def_republic_flag_trigger = yes
		}
	}
	flag_definition = {
		coa = A04_republic_dalr
		subject_canton = A04_republic_dalr
		priority = 11
		trigger = {
			coa_def_republic_flag_trigger = yes
			scope:target = {
				country_has_primary_culture = cu:dalr
			}
		}
	}
	flag_definition = {
		coa = A04_dictatorship
		subject_canton = A04_dictatorship
		priority = 20
		trigger = {
			coa_def_dictatorship_flag_trigger = yes
		}
	}
	flag_definition = {
		coa = A04_dictatorship_dalr
		subject_canton = A04_dictatorship_dalr
		priority = 21
		trigger = {
			coa_def_dictatorship_flag_trigger = yes
			scope:target = {
				country_has_primary_culture = cu:dalr
			}
		}
	}
	flag_definition = {
		coa = A04_dictatorship
		subject_canton = A04_dictatorship
		priority = 20
		trigger = {
			coa_def_oligarchy_flag_trigger = yes
		}
	}
	flag_definition = {
		coa = A04_dictatorship_dalr
		subject_canton = A04_dictatorship_dalr
		priority = 21
		trigger = {
			coa_def_oligarchy_flag_trigger = yes
			scope:target = {
				country_has_primary_culture = cu:dalr
			}
		}
	}
	flag_definition = {
		coa = A04_communist
		subject_canton = A04_communist
		priority = 1500
		trigger = {
			coa_def_communist_flag_trigger = yes
		}
	}
	flag_definition = {
		coa = A04_communist_dalr
		subject_canton = A04_communist_dalr
		priority = 1501
		trigger = {
			coa_def_communist_flag_trigger = yes
			scope:target = {
				country_has_primary_culture = cu:dalr
			}
		}
	}
}

A05 = { # Bisan
	flag_definition = {
		coa = A05
		subject_canton = A05
		allow_overlord_canton = yes
		coa_with_overlord_canton = A05_subject
		priority = 1
	}
}

A08 = { # Eborthil
	flag_definition = {
		coa = A08
		subject_canton = A08
		allow_overlord_canton = yes
		coa_with_overlord_canton = A08_subject
		priority = 1
	}
	flag_definition = {
		coa = A08_republic
		subject_canton = A08_republic
		priority = 10
		trigger = {
			coa_def_republic_flag_trigger = yes
		}
	}
	flag_definition = {
		coa = A08_communist
		subject_canton = A08_communist
		priority = 1500
		trigger = {
			coa_def_communist_flag_trigger = yes
		}
	}
}

A09 = { # Busilar
	flag_definition = {
		coa = A09
		subject_canton = A09
		allow_overlord_canton = yes
		coa_with_overlord_canton = A09_subject
		priority = 1
	}
	flag_definition = {
		coa = A09_republic
		subject_canton = A09_republic
		priority = 10
		trigger = {
			coa_def_republic_flag_trigger = yes
		}
	}
	flag_definition = {
		coa = A09_communist
		subject_canton = A09_communist
		priority = 1500
		trigger = {
			coa_def_communist_flag_trigger = yes
		}
	}
}


A10 = { # Grombar
	flag_definition = {
		coa = A10
		subject_canton = A10
		priority = 1
	}
	flag_definition = {
		coa = A10_communist
		subject_canton = A10_communist
		priority = 1500
		trigger = {
			coa_def_communist_flag_trigger = yes
		}
	}
	flag_definition = {
		coa = A10_fascist
		subject_canton = A10_fascist
		priority = 1500
		trigger = {
		coa_def_fascist_flag_trigger = yes
		}
	}
}

A11 = { # Deshak
	flag_definition = {
		coa = A11
		subject_canton = A11
		allow_overlord_canton = yes
		coa_with_overlord_canton = A11_subject
		priority = 1
	}
}

A14 = { #Small Country
	flag_definition = {
		coa = A14
		subject_canton = A14
		priority = 1
	}
	flag_definition = {
		coa = A14_absolute_monarchy
		subject_canton = A14_absolute_monarchy
		priority = 200
		trigger = {
			coa_def_absolute_monarchy_flag_trigger = yes
		}
	}
	flag_definition = {
		coa = A14_communist
		subject_canton = A14_communist
		priority = 1500
		trigger = {
			coa_def_communist_flag_trigger = yes
		}
	}
}

A32 = { # Silvermere
	flag_definition = {
		coa = A32
		subject_canton = A32
		priority = 1
	}
	flag_definition = {
		coa = A32_fascism
		subject_canton = A32_fascism
		priority = 1500
		trigger = {
			coa_def_fascist_flag_trigger = yes
		}
	}
}

A36 = { # Estallen
	flag_definition = {
		coa = A36
		subject_canton = A36
		priority = 1
	}
	flag_definition = {
		coa = A36_absolute_monarchy
		subject_canton = A36_absolute_monarchy
		priority = 200
		trigger = {
			coa_def_absolute_monarchy_flag_trigger = yes
		}
	}
}

A38 = { # Bennon
	flag_definition = {
		coa = A38
		subject_canton = A38
		priority = 1
	}
	flag_definition = {
		coa = A38_absolute_monarchy
		subject_canton = A38_absolute_monarchy
		priority = 20
		trigger = {
			coa_def_absolute_monarchy_flag_trigger = yes
		}
	}
}

A39 = { # Leslinpar
	flag_definition = {
		coa = A39
		subject_canton = A39
		priority = 1
	}
	flag_definition = {
		coa = A39_absolute_monarchy
		subject_canton = A39_absolute_monarchy
		priority = 20
		trigger = {
			coa_def_absolute_monarchy_flag_trigger = yes
		}
	}
}

A40 = { # Esmaraine
	flag_definition = {
		coa = A40
		subject_canton = A40
		priority = 1
	}
	flag_definition = {
		coa = A40_absolute_monarchy
		subject_canton = A40_absolute_monarchy
		priority = 20
		trigger = {
			coa_def_absolute_monarchy_flag_trigger = yes
		}
	}
}

A59 = { # Westmoors
	flag_definition = {
		coa = A59
		subject_canton = A59
		allow_overlord_canton = yes
		coa_with_overlord_canton = A59_subject
		priority = 1
	}
}

A69 = { # Esmaria
	flag_definition = {
		coa = A40
		subject_canton = A40
		priority = 1
	}
	flag_definition = {
		coa = A40_absolute_monarchy
		subject_canton = A40_absolute_monarchy
		priority = 20
		trigger = {
			coa_def_absolute_monarchy_flag_trigger = yes
		}
	}
}

A73 = { # Sorncost
	flag_definition = {
		coa = A73
		subject_canton = A73
		allow_overlord_canton = yes
		priority = 1
	}
}

B05 = { # Vanburia
	flag_definition = {
		coa = B05
		subject_canton = B05_subject_canton
		allow_overlord_canton = yes
		overlord_canton_scale = { 0.334 0.334 }
		priority = 1
	}
	flag_definition = {
		coa = B05_republic
		subject_canton = B05_subject_canton
		allow_overlord_canton = yes
		overlord_canton_scale = { 0.334 0.334 }
		
		priority = 10
		trigger = {
			coa_def_republic_flag_trigger = yes
		}
	}
	flag_definition = {
		coa = B05_guild
		subject_canton = B05_subject_canton
		allow_overlord_canton = yes
		overlord_canton_scale = { 0.334 0.334 }
		
		priority = 20
		trigger = {
			coa_def_guild_flag_trigger = yes
		}
	}
}

F14 = { # Overclan
	flag_definition = {
		coa = F14
		subject_canton = F14
		allow_overlord_canton = yes
		coa_with_overlord_canton = F14_subject
		overlord_canton_scale = { @[1/3] @[1/3] }
		priority = 1
	}
}