﻿#Cannor Elven States
ideology_elven_havenist = {
	icon = "gfx/interface/icons/ideology_icons/papal_paternalistic.dds"
	
	lawgroup_governance_principles = {
		law_monarchy = approve
		law_presidential_republic = approve
		law_theocracy = neutral
		law_magocracy = neutral
		law_parliamentary_republic = neutral
		law_council_republic = strongly_disapprove
	}
	
	lawgroup_distribution_of_power = {
		law_landed_voting = strongly_approve
		law_autocracy = strongly_approve
		law_oligarchy = approve
		law_technocracy = disapprove
		law_wealth_voting = neutral
		law_census_voting = disapprove
		law_universal_suffrage = strongly_disapprove
		law_anarchy = strongly_disapprove
	}
	
	lawgroup_bureaucracy = {
		law_hereditary_bureaucrats = approve
		law_appointed_bureaucrats = neutral
		law_elected_bureaucrats = disapprove
	}

	lawgroup_policing = {
		law_local_police = approve
		law_dedicated_police = neutral
		law_militarized_police = neutral
		law_no_police = disapprove
	}
	
	lawgroup_economic_system = {		
		law_traditionalism = strongly_approve
		law_agrarianism = approve
		law_interventionism = neutral
		law_laissez_faire = disapprove
		law_command_economy = strongly_disapprove
	}
	
	lawgroup_trade_policy = {		
		law_isolationism = strongly_approve
		law_mercantilism = approve
		law_protectionism = neutral
		law_free_trade = disapprove
	}	

	lawgroup_migration = {
		law_closed_borders = approve
		law_migration_controls = neutral
		law_no_migration_controls = disapprove
	}
	
	lawgroup_racial_tolerance = {
		law_same_race_only = approve
		law_giantkin_group_only = stongly_disapprove
		law_goblinoid_group_only = stongly_disapprove
		law_giantkin_group_and_humans = stongly_disapprove
		law_goblinoid_group_and_humans = stongly_disapprove
		law_monstrous_only = stongly_disapprove
		law_non_monstrous_only = disapprove
		law_all_races_allowed = strongly_disapprove
	}
}

#Ravelian Rectorate
ideology_rectorate_paternalistic = {
	icon = "gfx/interface/icons/ideology_icons/papal_paternalistic.dds"
	
	lawgroup_governance_principles = {
		law_theocracy = strongly_approve
		law_monarchy = approve		
		law_presidential_republic = neutral
		law_parliamentary_republic = neutral
		law_magocracy = disapprove
		law_council_republic = disapprove
	}
	
	lawgroup_distribution_of_power = {
		law_landed_voting = strongly_approve
		law_autocracy = strongly_approve
		law_oligarchy = approve
		law_wealth_voting = neutral
		law_census_voting = disapprove
		law_universal_suffrage = strongly_disapprove
		law_anarchy = strongly_disapprove
	}
	
	lawgroup_bureaucracy = {
		law_hereditary_bureaucrats = approve
		law_appointed_bureaucrats = neutral
		law_elected_bureaucrats = disapprove
	}

	lawgroup_policing = {
		law_local_police = approve
		law_dedicated_police = neutral
		law_militarized_police = neutral
		law_no_police = disapprove
	}
}

ideology_rectorate_plutocratic = {
	icon = "gfx/interface/icons/ideology_icons/papal_paternalistic.dds"
	
	lawgroup_distribution_of_power = {
		law_oligarchy = approve
		law_wealth_voting = approve
		law_landed_voting = neutral
		law_census_voting = neutral
		law_autocracy = disapprove
		law_universal_suffrage = strongly_disapprove
		law_anarchy = strongly_disapprove
	}

	lawgroup_governance_principles = {
		law_theocracy = approve
		law_monarchy = neutral			
		law_presidential_republic = neutral
		law_parliamentary_republic = neutral
		law_magocracy = disapprove
		law_council_republic = strongly_disapprove
	}
	
	lawgroup_taxation = {
		law_per_capita_based_taxation = approve
		law_consumption_based_taxation = neutral
		law_land_based_taxation = neutral
		law_proportional_taxation = neutral
		law_graduated_taxation = disapprove
	}	
	
	lawgroup_colonization = {
		law_colonial_exploitation = approve
		law_no_colonial_affairs = neutral
		law_colonial_resettlement = neutral
	}

	lawgroup_magic_and_artifice = {
		law_traditional_magic_only = strongly_disapprove
		law_traditional_magic_encouraged = disapprove
		law_artifice_encouraged = approve
		law_artifice_only = strongly_approve
		law_mundane_production = disapprove
	}
}

ideology_aldresian = {
	icon = "gfx/interface/icons/ideology_icons/papal_paternalistic.dds"

	lawgroup_governance_principles = {
		law_theocracy = strongly_approve
		law_monarchy = neutral			
		law_presidential_republic = neutral
		law_parliamentary_republic = neutral
		law_magocracy = disapprove
		law_council_republic = strongly_disapprove
	}
	
	lawgroup_magic_and_artifice = {
		law_traditional_magic_only = strongly_disapprove
		law_traditional_magic_encouraged = disapprove
		law_artifice_encouraged = approve
		law_artifice_only = approve
		law_mundane_production = neutral
	}
	
	lawgroup_mage_ethics = {
		law_dark_arts_banned = approve
		law_pragmatic_application = disapprove
		law_dark_arts_embraced = strongly_disapprove
	}

	lawgroup_artificer_ethics = {
		law_amoral_artifice_banned = approve
		law_pragmatic_artifice = disapprove
		law_amoral_artifice_embraced = strongly_disapprove
	}
}

#Devout Religion Ideologies
ideology_ravelian_moralist = {
	icon = "gfx/interface/icons/ideology_icons/orthodox_patriarch.dds"
	
	lawgroup_governance_principles = {
		law_monarchy = neutral
		law_theocracy = approve	
		law_presidential_republic = neutral
		law_parliamentary_republic = neutral
		law_magocracy = disapprove
		law_council_republic = strongly_disapprove
	}

	lawgroup_church_and_state = {
		law_state_religion = strongly_approve
		law_freedom_of_conscience = neutral
		law_total_separation = strongly_disapprove
		law_state_atheism = strongly_disapprove
	}

	lawgroup_citizenship = {
		law_ethnostate = strongly_disapprove
		law_national_supremacy = disapprove
		law_racial_segregation = neutral
		law_cultural_exclusion = approve
		law_multicultural = neutral	
	}
	
	lawgroup_magic_and_artifice = {
		law_traditional_magic_only = strongly_disapprove
		law_traditional_magic_encouraged = disapprove
		law_artifice_encouraged = approve
		law_artifice_only = approve
		law_mundane_production = neutral
	}
	
	lawgroup_mage_ethics = {
		law_dark_arts_banned = approve
		law_pragmatic_application = disapprove
		law_dark_arts_embraced = strongly_disapprove
	}

	lawgroup_artificer_ethics = {
		law_amoral_artifice_banned = approve
		law_pragmatic_artifice = disapprove
		law_amoral_artifice_embraced = strongly_disapprove
	}
}

ideology_scholarly = { #Replaces Pious for Ravelian + Thought?
	icon = "gfx/interface/icons/ideology_icons/russian_patriarch.dds"

	lawgroup_health_system = {
		law_charitable_health_system = approve
		law_public_health_insurance = neutral
		law_no_health_system = disapprove
		law_private_health_insurance = disapprove
	}
	
	lawgroup_education_system = {
		law_religious_schools = approve
		law_private_schools = approve
		law_no_schools = neutral
		law_public_schools = disapprove		
	}
}

ideology_corinite_moralist = {
	icon = "gfx/interface/icons/ideology_icons/orthodox_patriarch.dds"
	
	lawgroup_governance_principles = {
		law_monarchy = strongly_approve
		law_theocracy = approve	
		law_magocracy = approve
		law_presidential_republic = approve
		law_parliamentary_republic = disapprove
		law_council_republic = strongly_disapprove
	}
	
	lawgroup_distribution_of_power = {
		law_autocracy = strongly_approve
		law_oligarchy = approve
		law_landed_voting = neutral
		law_wealth_voting = neutral
		law_census_voting = disapprove
		law_universal_suffrage = strongly_disapprove
		law_anarchy = strongly_disapprove
	}

	lawgroup_church_and_state = {
		law_state_religion = strongly_approve
		law_freedom_of_conscience = neutral
		law_total_separation = strongly_disapprove
		law_state_atheism = strongly_disapprove
	}

	lawgroup_citizenship = {
		law_ethnostate = strongly_disapprove
		law_national_supremacy = disapprove
		law_racial_segregation = neutral
		law_cultural_exclusion = approve
		law_multicultural = neutral	
	}
	
	lawgroup_army_model = {
		law_mass_conscription = strongly_approve
		law_professional_army = approve
		law_peasant_levies = neutral
		law_national_militia = disapprove
	}
	
	lawgroup_free_speech = {
		law_protected_speech = approve
		law_right_of_assembly = neutral
		law_censorship = disapprove
		law_outlawed_dissent = strongly_disapprove
	}
	
	#Should these guys like less magical restrictions? Maybe they just don't care?
	lawgroup_mage_ethics = {
		law_dark_arts_banned = approve
		law_pragmatic_application = neutral
		law_dark_arts_embraced = disapprove
	}

	#TODO
	lawgroup_artificer_ethics = {
		law_amoral_artifice_banned = approve
		law_pragmatic_artifice = neutral
		law_amoral_artifice_embraced = disapprove
	}
}

ideology_adeanic_moralist = {
	icon = "gfx/interface/icons/ideology_icons/orthodox_patriarch.dds"
	
	lawgroup_governance_principles = {
		law_monarchy = strongly_approve
		law_theocracy = approve	
		law_magocracy = disapprove
		law_presidential_republic = neutral
		law_parliamentary_republic = neutral
		law_council_republic = strongly_disapprove
	}

	lawgroup_church_and_state = {
		law_state_religion = strongly_approve
		law_freedom_of_conscience = neutral
		law_total_separation = strongly_disapprove
		law_state_atheism = strongly_disapprove
	}

	lawgroup_citizenship = {
		law_ethnostate = neutral
		law_national_supremacy = approve
		law_racial_segregation = neutral
		law_cultural_exclusion = disapprove
		law_multicultural = strongly_disapprove	
	}
	
	lawgroup_racial_tolerance = {
		law_same_race_only = approve
		law_giantkin_group_only = neutral
		law_goblinoid_group_only = neutral
		law_giantkin_group_and_humans = neutral
		law_goblinoid_group_and_humans = neutral
		law_monstrous_only = neutral
		law_non_monstrous_only = neutral
		law_all_races_allowed = disapprove
	}
	
	lawgroup_mage_ethics = {
		law_dark_arts_banned = approve
		law_pragmatic_application = neutral
		law_dark_arts_embraced = disapprove
	}

	lawgroup_artificer_ethics = {
		law_amoral_artifice_banned = approve
		law_pragmatic_artifice = neutral
		law_amoral_artifice_embraced = disapprove
	}
}
