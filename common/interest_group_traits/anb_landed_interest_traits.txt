﻿ig_trait_family_ties_witch_king = {
	icon = "gfx/interface/icons/ig_trait_icons/family_ties.dds"
	min_approval = happy
	
	modifier = {
		building_group_bg_agriculture_throughput_mult = 0.05
		building_group_bg_ranching_throughput_mult = 0.05
		building_group_bg_plantations_throughput_mult = 0.05
	}
}